#!/usr/bin/env python

import os
import re
import subprocess
import git


useSvn = False
useGit = False

def is_in_git(path):
    try:
        print('Checking Git Status...')
        output = subprocess.check_output('git status ' + path,
                                         shell=True,
                                         stderr=subprocess.STDOUT,)

        if 'but untracked files present' in output:
            return True
        if 'deleted:' in output:
            return True
        if 'modified:' in output:
            return True
    except subprocess.CalledProcessError as e:
        print('*****************************************************')
        print('This document is not part of a Git Repository.')
        print('*****************************************************')
        return False
    return True

def dirty_build_git(path):
    try:
        output = subprocess.check_output('git status ' + path,
                                         shell=True,
                                         stderr=subprocess.STDOUT,)

        if 'but untracked files present' in output:
            print('******************************************')
            print('Your document is in Git but it is dirty')
            print('You have non-committed, new files in Git!')
            print('******************************************')
            return True
        if 'deleted:' in output:
            print('*********************************************')
            print('Your document is in Git but is dirty.')
            print('You have non-committed, deleted files in Git!')
            print('*********************************************')
            return True
        if 'modified:' in output:
            print('**********************************************')
            print('Your document is in Git but dirty.')
            print('You have non-committed, modified files in Git!')
            print('**********************************************')
            return True
        if 'nothing to commit, working tree clean' in output:
            return False
    except subprocess.CalledProcessError as e:
        return True
    return False

def is_in_svn(path):
    try:
        print('Checking SVN Status...')
        output = subprocess.check_output('svn pg svn:ignore ' + path,
                                         shell=True,
                                         stderr=subprocess.STDOUT,)

        if 'is not a working copy' in str(output):
            print('*****************************************************')
            print('***ERROR this is not a working SVN copy!***')
            print('*****************************************************')
            return False
    except subprocess.CalledProcessError as e:
        print('*****************************************************')
        print('This document is not part of a SVN Repository.')
        print('*****************************************************')
        return False
    print('**********************************************************************')
    print('All document files and folders are committted and non-modified in SVN.')
    print('**********************************************************************')
    return True

def get_expected_links(fname):
    # get_expected_links(str: fname) -> dict
    '''Reads the provided file to determine the links that should be created.

    Args:
        fname (str): The name of the file to read that contains the links file.

    Returns:
        dict: A mapping of relative path names of the link name to its target.
    '''
    links = {}
    links_file = open(fname)
    sb_links = links_file.readlines()
    links_file.close()
    remove_comments = re.compile('#.*$')
    remove_whitespace = re.compile('[\t ]+')
    for link in sb_links:
        link = remove_comments.sub('', link)
        link = remove_whitespace.sub(' ', link).strip()
        if link:
            fields = link.split(' ')
            if len(fields) != 2:
                print('Error parsing line: ' + link)
                continue
            links[fields[0]] = fields[1]
    return links


def find_links(dir):
    # find_links(str: dir) -> list
    '''Find all links in a given directory

    Args:
        dir (str): The top directory to start seaching in

    Returns:
        list: A :class:`list` of :class:`str` with relative paths from the
              current directory.
    '''
    # os.walk doesn't list links, at least on cygwin/windows, so we need to list
    # each dir and search for links manually.
    links = []
    for root, dirs, files in os.walk(os.path.abspath(dir),
                                     topdown=False,
                                     followlinks=False):
        for f in os.listdir(root):
            f = os.path.relpath(os.path.join(root, f))
            if os.path.islink(f):
                links.append(f)
    return links



def check_links():
    current_links = find_links('source')
    expected_links = get_expected_links('links')
    for link in expected_links.copy():
        if link not in current_links or os.path.relpath(os.path.realpath(link)) != expected_links[link]:
            print('Link {} does not exist or is incorrect, attempting to fix.'.format(link))
            create_link(link, expected_links[link])
        if link in current_links:
            current_links.remove(link)
        del expected_links[link]
    for link in current_links:
        print('Removing link {}'.format(link))
        os.remove(link)


def create_link(target, source):
    global useSvn,useGit
    if os.path.lexists(target):
        if not os.path.islink(target) and os.path.isdir(target):
            raise(ValueError('Unable to create link at {target}, a directory exists there.'.format(target=target)))
        print('Link {} is no longer in the links file, removing.'.format(target))
        os.remove(target)
    try:
        link_path = os.path.relpath(source, os.path.dirname(target))
        print('Creating link {} -> {}'.format(target, link_path))
        output = subprocess.check_output('export CYGWIN=winsymlinks; ln -s  {source} {target}'.format(source=link_path, target=target),
                                         shell=True,)
    except subprocess.CalledProcessError as e:
        raise ValueError('Unable to create link at {target}, ln -s returned {error}'.format(target=target,
                                                                                             error=e.output))
    if useSvn:
        print(target)
        link_dir = os.path.join(*target.split(os.path.sep)[:-1])
        link_name = target.split(os.path.sep)[-1]
        try:
            ignores = subprocess.check_output('svn pg svn:ignore ' + link_dir, shell=True).splitlines()
        except:
            ignores = []
        for ln in [ link_name, link_name + '.lnk' ]:
            if ln not in ignores:
                subprocess.check_output('svn propset svn:ignore "' + '\n'.join([x for x in ignores if x] + [ln]) +'" ' + link_dir, shell=True)

def cygwin_to_windows_path(path):
    path = os.path.abspath(path)
    path = path.replace('/cygdrive/', '')
    path = path.replace('/', ':/', 1)
    path = path.replace('/', '\\')
    return path





if __name__ == '__main__':
    if is_in_svn('.'):
        useSvn = True
        print('Using SVN: {useSvn}'.format(useSvn=useSvn))
        try:
            check_links()
        except Exception as e:
            print(e)
            exit(1)
    if is_in_git('.'):
        useGit = True
        git_repo = git.Repo(search_parent_directories=True)
        git_sha_full = git_repo.head.object.hexsha
        if dirty_build_git('.'):
            git_untrackedFiles = git_repo.untracked_files
            git_dirtyFiles = [ item.a_path for item in git_repo.index.diff(None) ]
            print('Using Git: {useGit}'.format(useGit=useGit))
            print('*****************************************************')
            print('The build is marked dirty due to the following files:')
            print('\n'.join(git_dirtyFiles))
            print('*****************************************************')
            print('Git SHA: ' + git_sha_full)
            print('*****************************************************')
        else:
            print('**********************************************************************')
            print('Using Git: {useGit}'.format(useGit=useGit))
            print('**********************************************************************')
            print 'All document files and folders are committted and non-modified in Git.'
            print('**********************************************************************')
            print('Git SHA: ' + git_sha_full)
            print('**********************************************************************')
        try:
            check_links()
        except Exception as e:
            print(e)
            exit(1)
    check_links()
