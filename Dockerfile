# escape=`
FROM alpine:3.7

MAINTAINER Josh Johnson <binarylandscapes@outlook.com>

# Installs the listed packages using Alpine Package Keeper
RUN apk add --no-cache --virtual --update `
    py-pip `
    make `
    wget `
    ca-certificates `
    ttf-dejavu `
    openjdk8-jre `
    graphviz

# Upgrades installed Python pip package
RUN pip install --upgrade `
    pip

# Install Python packages using pip
RUN pip install `
    sphinx `
    sphinx_rtd_theme `
    sphinxcontrib-plantuml `
    sphinx_autobuild

# Download PlantUML Java application and set a command for it
RUN wget http://downloads.sourceforge.net/project/plantuml/plantuml.jar -P /opt/
RUN echo -e '#!/bin/sh -e\njava -jar /opt/plantuml.jar "$@"' > /usr/local/bin/plantuml
RUN chmod +x /usr/local/bin/plantuml

WORKDIR /sphinx_doc

COPY template_document /sphinx_doc/template_document

EXPOSE 8000 35729
