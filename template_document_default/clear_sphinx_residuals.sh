#!/bin/bash
# Removes all softlinks from location ran recursivly.
# Removes build folder
# Removes all .pyc and .stackdump files
# Helpful to clean out Sphinx leftovers in folder

find . -type l -exec rm {} \;
find . -type f -name '*.pyc' -exec rm {} \;
find . -type f -name '*.stackdump' -exec rm {} \;
rm -rf ./build
