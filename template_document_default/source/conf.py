# -*- coding: utf-8 -*-
#
# This file is execfile()d with the current directory set to its
# containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import codecs
import datetime
import os
from pprint import pprint
import re
import string
import sys

import yaml

from yaml_config import yaml_config_parser

sys.path.insert(0, os.path.abspath('.'))
sys.path.append(os.path.abspath('./sphinx_python_scripts'))
sys.path.append(os.path.abspath('./sphinx_python_scripts/sphinxcontrib'))

from sphinx_helper import *


################################################################################
# General information about the project.
project = u'Template Document Title'   # No you can't have a \ or / in a file name
copyright = u'{}, Northrop Grumman Corp'.format(datetime.datetime.now().year)
author = u'Northrop Grumman Corp'
# Autodetermine the doc number from the path
documentnumber = 'template'
try:
    documentnumber = re.search('(?P<doc_number>[A-Z]*[0-9A-Z]{4}P[0-9A-Z]{4})', os.path.abspath('.')).groupdict().get('doc_number', '?????')
except:
    pass
document_rev = "NC"

# To Change Classification edit the classification and use links files to point ./latex_templates/program to common/latex_templates/$program files of:
# ap418 = Unclassified/NGMN with STS title logo
# ap922 = Unclassified/NGMN with CMCC title logo
# default = Unclassified/NGMN with NG title logo
# triton = Unclassified/FOUO with Triton title logo
# sphinx = Unclassified/NGMN with Sphinx title logo

classification = "UNCLASSIFIED"
latex_additional_files = ['./program_template/ngc_procedure.sty',
                          './program_template/titlelogo.png',
                          #'common/templates/iftex.sty',
                         ]


# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
# Get the version and release from the path of this document
version = ''
release = ''
try:
    path_matches = re.search('(?P<version>[A-Z]+[0-9]+[0-9](\.[0-9]+[0-9])+).*/(?P<release>[A-Z]+[0-9]+(\.[0-9]+)+)/', os.path.abspath('.'))
    version = path_matches.groupdict().get('version', '')
    release = path_matches.groupdict().get('release', '')
except:
    print('Warning: Unable to determine version/release variables from path')

################################################################################
# If you are going to use jinja content in your document, setup the env here:
# A context is a 3 or 4 part tuple with the following fields
#   ('Context Descriptive Name', 'short_context_name',
#    'configuration_files_dir', 'variables_file.yml)
# The variables.yml file is only required if it is used but not in the
# configurations_files_dir.
contexts = [('','','.'),   # This needs to be here.  If you have not contexts, leave an "empty" one.
            ]
################################################################################

# -- Options for Confluence output ----------------------------------------------
confluence_publish = False
confluence_space_name = 'GSEG+Deployment'
confluence_parent_page = 'IFC 4.0 Development'
confluence_server_url = 'http://10.10.31.252/confluence/'
confluence_server_user = 'username'
confluence_server_pass = 'password'

# Load the rest of the default configuration
exec(open(r'./sphinx_python_scripts/defaults.py').read())

# Add any code here required to override default values.
