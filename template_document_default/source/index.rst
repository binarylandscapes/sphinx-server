.. Leave the following heading here, Sphinx will swallow the first heading found
.. in pdf output, but it will be displayed in HTML

================================================================================
|project|
================================================================================


.. only:: html

    .. image:: /common/latex_templates/default/titlelogo.png
       :scale: 80
       :alt: Title Logo
       :align: center

    ---------------------
    Table of Contents
    ---------------------

.. toctree::
    :maxdepth: 4
    :numbered:
    :name: mastertoc
    :glob:

    scope
    how_to_use_this_document
    document/main_content/*

.. raw:: latex

    \beginappendices



.. only:: html

    ---------------------
    Table of Appendices
    ---------------------

.. toctree::
   :maxdepth: 2
   :numbered:
   :name: masteratoc
   :glob:

   common/appendices/acronym_list
   common/appendices/git_changelog
   document/appendices/*


.. These don't make any sense for PDF, so only generate them for HTML

.. only:: html

    ---------
    Indexes
    ---------

    * :ref:`genindex`
    * :ref:`search`

.. only:: html

    .. include:: common/templates/sig_table.inc

    .. include:: common/templates/rev_history.inc
