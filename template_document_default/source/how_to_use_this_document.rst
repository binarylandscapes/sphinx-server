===================================
How to Use This Document
===================================

.. toctree::
    :glob:

    procedural_flow
    common/boiler_plate/device_machine_access_methods
    common/boiler_plate/login_to_a_zone
