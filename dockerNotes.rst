Images

    .. To create a Docker image from current folder contents with Dockerfile configuration
        .. docker build --tag <repositoryFolder/repositoryName>:<tag>
    docker build --tag binarylandscapes/sphinx-server:dev .\

    .. To list created or pulled Docker images
    docker image ls -a

    .. To delete a Docker image
        docker image rm <image id>

Containers

    .. To create a Docker container from an image and be ready to start
        docker create --interactive|-i --tty|-t --name <container name> <repository>:<tag>

    .. To list created containers
    docker container ls -a

    .. Automatically remove the container once it exits
        --rm

    .. Publish the containers exposed ports to the host
        --publish

    .. Mount a filesystem to the container
        --mount

    .. Set meta data on a container
        --label

    .. Add host folder to container under as container folder
        --volume <host folder>:<container folder>

    .. Full command to create from an image
    docker create --name sphinx-server --publish 8000:8000 binarylandscapes/sphinx-server:dev

    .. To create and run a Docker container from an image
        .. docker run --interactive|-i --tty|-t --name <container name> <repository>:<tag>
    docker run --tty --interactive --name sphinx-server --publish 8000:8000 --rm binarylandscapes/sphinx-server:dev

    .. Start a created Docker container and open an interactive TTY session to it
        .. docker start --attach --interactive <container id>
    docker start --attach sphinx-server

    .. To list running containers
    docker ps -a

    .. To connect a shell session to a running container
    docker exec
