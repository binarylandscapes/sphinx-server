==================
Required Materials
==================

******************
Required Equipment
******************

.. _Required-Equipment:

.. tabularcolumns:: |p{\dimexpr 0.3\linewidth-2\tabcolsep}
                    |p{\dimexpr 0.6\linewidth-2\tabcolsep}|

.. .. required_equipment_list::

.. list-table:: Required Equipment
    :header-rows: 1
    :class: longtable

    * - **Part Number**
      - **Description**
    * - N/A
      -

******************
Required Documents
******************

.. _Required-Documents:

.. list-table:: Required Documents
   :header-rows: 1
   :class: longtable

   * - Document Number
     - Description
   * - N/A
     -

.. raw:: latex

    \newpage

******************
Required Media
******************

The following media is required:

.. _Required-Media:

.. tabularcolumns:: |p{\dimexpr 0.3\linewidth-2\tabcolsep}
                    |p{\dimexpr 0.6\linewidth-2\tabcolsep}|

.. required_media_list::

.. raw:: latex

    \newpage
