===================
Reference Documents
===================

.. _Reference-Documents:

.. list-table:: Reference Documents
   :header-rows: 1
   :class: longtable

   * - Document Number or Link
     - Description
   * - `<http://www.sphinx-doc.org/en/1.7/>`_
     - Sphinx Documentation for v1.7
   * - `<http://jinja.pocoo.org/docs/2.10/>`_
     - Jinja 2 Documentation for v2.10
   * - `<https://www.latextemplates.com/svgnames-colors>`_
     - *svgnames* Colors from *xcolor* package for Latex

.. raw:: latex

    \newpage
