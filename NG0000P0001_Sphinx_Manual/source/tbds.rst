======================================
To Be Determined (TBDs)
======================================

Any TBDs in this document are noted here.

.. tabularcolumns:: |p{\dimexpr 0.2\linewidth-2\tabcolsep}|p{\dimexpr 0.4\linewidth-2\tabcolsep}|p{\dimexpr 0.4\linewidth-2\tabcolsep}|

.. tbdlist::

.. raw:: latex

    \newpage
