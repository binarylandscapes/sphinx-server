:orphan:

===================
Procedural Flow
===================

This procedure is written to be executed from start to finish for the system you are running it on. You will need a copy of this procedure for each system you are performing it on. This signed document should be scanned and archived with system documentation as a record of the steps that have been performed for CM tracking.

Some sections may only apply to one system; ensure you read over an entire section before proceeding.

Some sections may have code block with commands to directly copy and paste to console to ease installation and reduce human error. Ensure you read over entire section before proceeding to confirm you are at the correct console prompt.

.. note::

    Use of the code blocks for copy and paste operations **does not remove** the requirement of a printed and signed copy of completed procedure for each system.
