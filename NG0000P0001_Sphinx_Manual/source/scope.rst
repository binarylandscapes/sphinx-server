=====
Scope
=====

This document provides an overview to the Sphinx documentation method including installation, syntax and getting started from a template structure.

.. important::

    The majority of the credit for the :program:`Cygwin` environment installation with Sphinx, Latex and custom scripting to support multiple yaml files goes to Peter Burdine <peter.burdine@ngc.com>. This manual is a gathering of knowledge and lessons learned from a trial by fire of the NGAS AP-418 program of doing technical documentation more efficiently. I took that framework with me to NG AP-922, and started modifying the environment to support Git and other small improvements in order to bring this to Triton such as adding diagrams. The goal is to have this become the technical documentation method for Northrop Grumman as a whole with one common configuration management repository to pull from for any and all NG programs across sectors for commonality, culture and efficiency.

.. toctree::
   :maxdepth: 2

   prerequisites
   common/boiler_plate/document_conventions
   required_materials
   reference_documents
   tbds
   common/boiler_plate/safety_precautions
