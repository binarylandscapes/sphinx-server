======================
Sphinx Directives
======================

.. toctree::
    :maxdepth: 4
    :glob:

    image_figure
    note_type
    table
    code_literal
    specialized
    latexpdf
    custom
    blockdiag/contents
    plantuml/plantuml

.. raw:: latex

    \newpage
