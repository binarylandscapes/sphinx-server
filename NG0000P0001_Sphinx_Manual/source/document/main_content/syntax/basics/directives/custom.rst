.. jinja:: yaml_config

    -----------------------------------------------
    Custom Directives (Not part of Sphinx baseline)
    -----------------------------------------------

    *   Custom roles via downloadable Sphinx Extensions

        *   Adding excerpts from git history to documentation `<https://github.com/OddBloke/sphinx-git>`_

            *   Requirements:

                * Install: ``sphinx-git`` via Python :program:`pip` (Included in :media:`SPHINXDVD0001`, Sphinx Environment)
                * Add ``sphinx-git`` to Sphinx extentions in conf.py (Included in :media:`SPHINXDVD0001`, Sphinx Environment via defaults.py)

            *   Syntax:

                ``.. git_changelog::``

            *   Rendered (Git Changelog of this document):

            {% if 'inGit' in gitstatus %}

                .. git_changelog::

            {% else %}

                **Not rendered due to document not being part of a Git repository.**

            {% endif %}

            .. raw:: latex

                \newpage

        *   Generate documentation from a task in an Ansible Playbook. `<https://github.com/shirou/sphinxcontrib-ansibleautodoc>`_

            .. code-block:: none

                .. ansibleautotask:: second task
                    :playbook: ../ansible/web.yml

            Rendered:

                :tbd:`Add rendered section when have an example playbook`

        *   Display a JSON Schema in your documentation `<https://github.com/lnoor/sphinx-jsonschema>`_

            .. code-block:: none

                .. jsonschema:: /scripts/vmware/vcenter/triton_vCSA_on_ESXi_template.json

            Rendered: :tbd:`Still in work on this one`

            .. jsonschema:: /scripts/vmware/vcenter/triton_vCSA_on_ESXi_template.json

            .. only:: html

                See :download:`Example JSON Download Link </scripts/vmware/vcenter/triton_vCSA_on_ESXi_template.json>`


    .. raw:: latex

        \newpage
