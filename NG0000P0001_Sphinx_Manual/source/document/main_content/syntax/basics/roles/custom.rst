-----------------------------------------------
Custom Roles (Not part of Sphinx baseline)
-----------------------------------------------

*   Northrop Grumman generated custom roles <peter.burdine@ngc.com> (Included in :media:`SPHINXDVD0001`, Sphinx Environment):

	*   Use this if you know something goes in the section, but aren't sure what that might be yet. This will be automatically added to TBD table of rendered document.

		* Syntax: ``:tbd:`To be determined```
		* Rendered: :tbd:`To be determined`

	*   This is used to reference an entry in the :file:`common/media.yaml` file to return the correct dash number associated with the part number entered. It also automatically adds it to to the Required Media table of rendered document.

		* Syntax: ``:media:`SPHINXDVD0001```
		* Rendered: :media:`SPHINXDVD0001`


.. raw:: latex

	\newpage
