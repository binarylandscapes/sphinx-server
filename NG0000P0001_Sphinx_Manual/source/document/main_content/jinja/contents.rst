*******************************************
Jinja Overview & Sphinx Syntax
*******************************************

.. toctree::
    :maxdepth: 4
    :glob:

    python_data_structures
    yaml
    examples/contents
