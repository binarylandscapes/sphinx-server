======================
YAML Basic Examples
======================

#.  Open :program:`Cygwin`
#.  :kbd:`cd /SPHINX01.01/SPHINX01.01`
#.  :kbd:`python` to open the program and practice along with the examples:

    *   Loads yaml_config with alias "ycp" and pprint modules
    *   Configures "y" to equal running ycp to load yamls from configuration_files folder
    *   Uses "y" to load files
    *   y.load_configuration()
    *   "c" now is the entire dict of all yamls in folder

        .. code-block:: pycon

            from yaml_config import yaml_config_parser as ycp
            from pprint import pprint
            y = ycp('configuration_files')
            y.load_configuration()
            c = y.config

#.  Displays the Drawing Number and Assembly Number from hardware.yaml for first item in list:

    .. code-block:: pycon

        print((c['hardware'][0]['ngDrawingInfo']['number'])+(c['hardware'][0]['ngDrawingInfo']['assy']))

#.  Displays the Name, Drawing Number and Assembly Number from hosts.yml which is linked to hardware.yml for first item in list:

    .. code-block:: pycon

        print((c['hosts'][0]['name'])+', '+(c['hosts'][0]['physical_host']['ngDrawingInfo']['number'])+(c['hosts'][0]['physical_host']['ngDrawingInfo']['assy']))

#.  Prints a list of concentated Drawing-Assy values from hardware.yml:

    .. code-block:: pycon

        [ pprint((h['ngDrawingInfo']['number'])+(h['ngDrawingInfo']['assy'])) for h in c['hardware'] ]

#.  Sets d equal to a list of concenated Drawing-Assy values from hardware.yaml. Then call a specific iterator with d[#]:

    .. code-block:: pycon

        d = [ (h['ngDrawingInfo']['number'])+(h['ngDrawingInfo']['assy']) for h in c['hardware'] if h.get('ngDrawingInfo')  ]


.. raw:: latex

    \newpage


======================
Jinja Basic Examples
======================

.. note::

    This is where everything comes toghether and is the entire basis for using Sphinx, Jinja and YAML. With the overall goal is to have a document template for multiple different systems via yaml files. This could be used system wide, program wide, sector wide and even NG wide.

``{# comment #}`` - Jinja Comment
``{% Jinja scripting call %}``

*   This is your **required** header to put at the top of every rst file you wish to use Jinja with and then indent all of the following content in line with ``{% set global...}``

    .. code-block:: none

        .. jinja:: yaml_config

            {% set globals = {'targets': []} %}
            {% import '/common/jinja_macros/common_macros.jinja' as common_macros %}

*   **Example:** Build a list of VMware ESXi Machines from the loaded YAML files via your :file:`conf.py` configured contexts

    .. code-block:: none

        {# Build a list of VMware ESXi Machines #}
        {% for h in hosts | selectattr('os.name', 'equalto', 'ESXi')   %}
            {% set _ = globals.update({'targets': globals['targets'] + [h]}) %}
        {% endfor %}

*   **Example:** Build a list of Physical hosts from the loaded YAML files via your :file:`conf.py` configured contexts

    .. code-block:: none

    	{# Build a list of Physical Hosts #}
        {% for h in hosts | selectattr('type', 'equalto', 'Physical')   %}
            {% set _ = globals.update({'targets': globals['targets'] + [h]}) %}
        {% endfor %}

*   **Example:** Create a table based on content from loaded YAML files with a for then if loop. The table will only be built if there is atleast (1) physical host and it requires no updates to be used with multiple different documents with different yaml files of same structure but different content. *Example does not include required Jinja header*

    .. important::

        You must ensure your dict of the YAML have all the same attributes. For example in the following yaml if any (1) of the hosts that are physical is missing a ``{{ h['ngcDrawingInfo']['number'] }}`` number attribute then the document build will fail. This doesn't mean it has to have a value for attribute it could be blank but it must be a matching structure for all ``h of hosts``. It is best practice to use template setup and import them to avoid this.

    .. note::

        Take note of the white spaces for the different Jinja variable. With no blank line between the lines, it auto adds a space but considers them on the same line. But this eases the readiability. But in the case of number and assy, you dont want a space so you must have them directly after each other or use a ``\`` as shown to cancel out the space.

    .. code-block:: none

        {# Build a list of Physical Hosts #}
        {% for h in hosts | selectattr('type', 'equalto', 'Physical')   %}
            {% set _ = globals.update({'targets': globals['targets'] + [h]}) %}
        {% endfor %}

        {% for h in globals['targets'] %}

            {% if loop.first %}

        ****************************************************************************
        Triton - {{ version }} - COE Layer 1 (Physical Hosts), HSCM - {{ release }}
        ****************************************************************************

        .. tabularcolumns:: |>{\RaggedRight}p{\dimexpr 0.2\linewidth-2\tabcolsep}
                            |>{\RaggedRight}p{\dimexpr 0.45\linewidth-2\tabcolsep}
                            |>{\RaggedRight}p{\dimexpr 0.35\linewidth-2\tabcolsep}|

        .. list-table::
            :header-rows: 1
            :class: longtable
            :widths: 20,45,35

            * - **Hostname**
              - **Version**
              - **Hardware**

            {% endif %}

            * - {{ h['name'] }}
              - {{ h['os']['manufacture'] }}
                {{ h['os']['name'] }}
                {{ h['os']['version'] }}
                Build {{ h['os']['build'] }}
              - NGC PN:
                {{ h['ngDrawingInfo']['number'] }}\
                {{ h['ngDrawingInfo']['assy'] }}

        {% endfor %}

.. raw:: latex

    \newpage
