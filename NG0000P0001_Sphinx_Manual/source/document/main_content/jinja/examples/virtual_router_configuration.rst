{# required for all reST files #}
:orphan:

{# not required for all reST files. only required
   if have to access the yaml config #}
.. jinja:: yaml_config 
    
    {# due to jinja scoping, globals is defined at the beginning of each file.
       targets isn't necessary, but found in most files. heading_level is
       required for generate_heading macro #}
    {% set globals = {'targets': [],
                      'heading_level': 0,} %}
                      
    {# common macros includes the applicable systems and sign-off blocks #}
    {% import '/common/jinja_macros/common_macros.jinja' as common_macros %}
        
    {# main section heading not using the generate_heading macro #}
    ##############################################################
    ``extr01`` Solaris Virtual Router Configuration
    ##############################################################

    {# check if added dictionary exists #}
    {% if added %}
    
        {# for each VM in the added['vm'] list if the hostname is equal to 'extr01' #}
        {% for v in added.get('vm', []) | selectattr('guest.hostname', 'equalto', 'extr01') %}
        
            {# jinja files are written generically, typically 'h' while iterating hosts and
               'v' while iterating VMs. so here we need to set h equal to the hostname (guest)
               definition of the VM to allow the jinja files to not throw an undefined variable error #}
            {% set h = v.guest %}
    
    {# this heading definition: 
        heading_level = 1
            the second heading level (0-indexed list)
         
        title = Deploy extr01 on xsim01
            this uses text formatting, see the back-ticks
            
        True (display title link in HTML)
    #}            
    {{ common_macros.generate_heading(1,
                                      'Deploy ``extr01`` on ``xsim01``',
                                      True) }} 
                                      
            {# jinja files should contain the jinja macro generate_heading, and the heading level is
               defined with the globals['heading_level'] variable. so define a value before including
               the jinja files #}
            {% do globals.update({'heading_level': 2}) %}
            
    {# include multiple sections that require specific arguments. in this case, each file
       requires the h object and globals['heading_level']. each jinja file should explain in
       its docstring #}
    {% include "/common/vmware_source/deploy_vm_to_esxi.jinja" %}
    {% include "/common/unix_source/solaris_system_identification.jinja" %}
    {% include "/common/unix_source/configure_networking.jinja" %}
    {% include "/common/unix_source/perform_general_solaris_os_configuration.jinja" %}
    {% include "/common/unix_source/patch_solaris_os_single.jinja" %}
    {% include "/common/unix_source/reboot_single_server.jinja" %}
    {% include "/common/unix_source/verify_patch_and_update_single.jinja" %}
    
    
    {# TBDs are used when a section is incomplete or an error occurred. in this case, it's suggesting
       more information is required. #}
    :tbd:`Need Solaris virtual router configuration` **Need Solaris virtual router configuration**
    
        {% else %}
        
        {# if there are not items in the addd['vm'] list then for-loop 
           is false and continue to this else-statement #}        
    ``extr01`` configuration not required on this system.
    
        {% endfor %}
        
    {% else %}
    
    {# error checking is also another reason to use if-statements.
       use a TBD role to flag any errors or missing sections and be sure to include
       a statement (typically duplicate text as TBD) so it's easy to notice the
       error message.  #}
    
    :tbd:`added.yml not found`
    **added.yml not found -- must add ``extr01`` to vm, host, and ip in added.yml**
    
    {% endif %}
    
    {# insert page break #}
    .. raw:: latex
    
        \newpage
        