*******************************************
Jinja Examples
*******************************************

.. toctree::
    :maxdepth: 4
    :glob:

    yaml_jinja_basics
