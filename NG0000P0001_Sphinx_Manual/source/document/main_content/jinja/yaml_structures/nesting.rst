###################################
Nesting Data Structures
###################################

YAML allows for nesting of mappings and lists.  Consider the following YAML:

.. code-block:: none

    my_list_of_servers:
    - server1
    - server2
    - server3

This will create the following datastructure::

    {'my_list_of_servers': ['server1', 'server2', 'server3']}

There are a few things to note in this example:

#.  When creating a mapping with nested structures, there should be no value on the same line as the mapping, instead it goes underneath.
#.  When using a nested collection (list), the dashes (``-``) need only be indented to the level of the mapping node, they can however be indented more.  If they are indented more, they all need to be indented to the same depth.

    .. code-block:: none

        my_list_of_servers:
            - server1
            - server2
            - server3

Mappings can also be nested along with collections.  Let's expand on the data structure we stared above and start filling in the definition of our servers:

.. code-block:: none

    my_list_of_servers:
    - hostname: server1
      os: win7
      roles:
      - whatever server1 does
    - hostname: server2
      os: win2012
      roles:
      - whatever server2 does
    - hostname: server3
      os: solaris11
      roles:
      - whatever server3 does

Once loaded, it would produce the following data structure:

.. code-block:: none

    {'my_list_of_servers': [{'hostname': 'server1',
                             'os': 'win7',
                             'roles': ['whatever server1 does']},
                            {'hostname': 'server2',
                             'os': 'win2012',
                             'roles': ['whatever server2 does']},
                            {'hostname': 'server3',
                             'os': 'solaris11',
                             'roles': ['whatever server3 does']}]
    }


While this looks confusing at first, it is just a nested data structure.  In this case it is a mapping with a list, which contains more mappings, one of which is a list.  For example, assume that it is loaded into the variable ``p``:


.. code-block:: none

    >>> p['my_list_of_servers'][0]['os']
    'win7'
    >>> server2 = p['my_list_of_servers'][1]
    >>> server2['roles']
    ['whatever server2 does']


.. raw:: latex

    \newpage
