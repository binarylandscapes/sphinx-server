############################################
YAML Merge Operator
############################################

YAML includes the ability to merge data from an anchor into the current key using the merge operator (``<<:``).  This can be useful for creating templates of objects that share common data.  For example, consider the following definition of two Windows 2012 VMs:

.. note::

    For simplicity's sake, some of the links have been removed and this structure has been simplified.

.. code-block:: none

    vms:
    - &dcserver01_vm
    name: dcserver01.<domain>.local
    template: server2012
    os_version: server2012
    guestId: vim.vm.GuestOsDescriptor.GuestOsIdentifier.windows8Server64Guest
    version: 'vmx-08'
    folder: <vm_folder>
    datastore: vmserver01_datastore_01
    guest: dcserver01_host
    host: <vm_host_01>
    hardware:
       numCPU: 4
       numCoresPerSocket: 4
       memoryMB: 16384
       device:
       - device1
       - device2
       - device3

    - &dcserver02_vm
    name: dcserver02.<domain>.local
    template: server2012
    os_version: server2012
    guestId: vim.vm.GuestOsDescriptor.GuestOsIdentifier.windows8Server64Guest
    version: 'vmx-08'
    folder: <vm_folder>
    datastore: vmserver01_datastore_01
    guest: dcserver02_host
    host: <vm_host_01>
    hardware:
       numCPU: 4
       numCoresPerSocket: 4
       memoryMB: 16384
       device:
       - device1
       - device2
       - device3

If you look carefully at the definitions of the two VMs, you will see see that they differ on 2 lines only, but each definition is made up of 17 lines.  If we use the merge operator, we can simplify this by doing the following:


.. raw:: latex

    \newpage


.. code-block:: none

    vm_templates:
    - &dcserver_template_vm
    name:
    template: server2012
    os_version: server2012
    guestId: vim.vm.GuestOsDescriptor.GuestOsIdentifier.windows8Server64Guest
    version: 'vmx-08'
    folder: <vm_folder>
    datastore: vmserver01_datastore_01
    guest:
    host: <vm_host_01>
    hardware:
       numCPU: 4
       numCoresPerSocket: 4
       memoryMB: 16384
       device:
       - device1
       - device2
       - device3
    vms:
    - &dcserver01_vm
    <<: *dcserver_template_vm
    name: dcserver01.<domain>.local
    guest: dcserver01_host

    - &dcserver02_vm
    <<: *dcserver_template_vm
    name: dcserver02.<domain>.local
    guest: dcserver02_host

If you have a few (or more) definitions that are almost identical that then can greatly shorten configuration files and make them much more consistent.  You must take care when using this method.  You may recall that if you enter a key a second time, that it overwrite the entire structure.  This can happen here.  For example, suppose on ``dcserver02_vm`` you wanted to change the amount of RAM, so you think "I'll just add those few lines" and you enter the following:

.. code-block:: none

    - &dcserver02_vm
    <<: *dcserver_template_vm
    name: dcserver02.<domain>.local
    guest: dcserver02_host
    host: <vm_host_01>
    hardware:
        memoryMB: 4096

If you did the above, the ONLY entry under ``hardware`` would be ``memoryMB``, everything else would be removed.  This is probably not what you wanted.  You may think that you can work around this by moving the merge line below to the last line like so:

.. code-block:: none

    - &dcserver02_vm
    name: dcserver02.<domain>.local
    guest: dcserver02_host
    host: <vm_host_01>
    hardware:
        memoryMB: 4096
    <<: *dcserver_template_vm


.. raw:: latex

    \newpage


This won't work either.  The merge operator will only merge keys that aren't already defined.  So when it goes to merge the data under ``hardware`` from ``dcserver_template_vm``, it sees that there is a ``hardware`` key already defined, so it will skip it.  So if you use merge operator and redefine part of a data structure, you may need to redfine the whole structure.  This is per the YAML specification, so there is no work around for it.

At the time of this writing, there is no way to extend a collection in YAML with links, anchors, or merging.  May people see the merge operator and think they can use it to create a short list that can be appended later.  This is not possible.  Trying to do so will result in either (1) a syntax error because you are mixing a mapping (which a merge is) with a collection  at the same level or (2) creating a multilevel list when you intended to have a single level list.


.. raw:: latex

    \newpage
