##################################################
Collections
##################################################

YAML allows for ``collections`` or ``lists``.  These are identified by a line starting with a dash (``-``) followed by a space.  When using a collection, this will map to a Python list.

Here is an example of a YAML collection:

.. code-block:: none

    - item1
    - item2
    - item3
    - 4

If this YAML file was loaded in to Python, it would create a list with four (4) items, three (3) strings and one (1) integer.

.. code-block:: python

    ['item1', 'item2', 'item3', 4]

This demonstrates when YAML is loaded it will try to auto determine the data type of each value.  This will be shown in more detail later.

Collections can be nested if required.  The syntax is note the start of the collection with a dash (``-``) followed by a space after the first one.  Then each item in the sub-collection is indented to the level of the new list.  Here is an example:

.. code-block:: none

    - - sublist1_item1
      - sublist1_item2
      - sublist1_item3
    - - sublist2_item1
      - sublist2_item2
      - sublist2_item3
      - - sublist2_sublist1_item1
        - sublist2_sublist1_item2

This would result in the following nested lists:

.. code-block:: python

    [['sublist1_item1', 'sublist1_item2', 'sublist1_item3'],
     ['sublist2_item1',
      'sublist2_item2',
      'sublist2_item3',
      ['sublist2_sublist1_item1', 'sublist2_sublist1_item2']]]

In practicality, there isn't a known limit to the number of nested lists.


.. raw:: latex

    \newpage
