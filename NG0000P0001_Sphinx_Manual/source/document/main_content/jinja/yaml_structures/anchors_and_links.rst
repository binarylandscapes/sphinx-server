##################################
YAML Anchors and Links
##################################

One of the ways that YAML saves time and reduces the amount of data that needs to entered is with Anchors and Links.  In the simpliest case, we can create a mapping with an anchor, then create a mapping that uses a link to retrieve the value.

.. code-block:: none

    key1: &anchor_name value1
    key2: *anchor_name

You can see in the above snippet, that an anchor is created when the amperstand (``&``) character is used followed by the name of the anchor.  In this case the anchor name is ``anchor_name``.  This can be any string that doesn't contain white space.  To reference the anchor, you use a link.  A link is created when you use the splat (``*``) character followed by the anchor name.

.. important::

    Since we are currently using the pyyaml library, there is a very important limitation, all anchors must be defined before they can be used.  This means all links must appear after the anchor is defined.  In the snippet above, this condition is met, but if the two lines were reversed, the parser would report an error.

.. note::

    If you have a programming background, this is creating a pointer to the memory space occupied by the data where the anchor is defined (it is not a copy of the data).  If you don't understand this, it is ok.

Now that the snippet is explained, let us take a look at the resulting data structure::

    {'key1': 'value1',
     'key2': 'value1'}

You can see that the value for ``key2`` was set to the same value as ``key1``.  The anchor names, are just that, names.  Thus they are not shown anywhere in the resulting data structure.

Now we can take a look at a more complicated data structure:

.. code-block:: none

    controller0: &lsi_controller_0
        key: 1000
        type: vim.vm.device.VirtualLsiLogicController
        sharedBus: vim.VirtualSCSISharing.noSharing
    controller1: &lsi_controller_1
        key: 1001
        type: vim.vm.device.VirtualLsiLogicController
        sharedBus: vim.VirtualSCSISharing.physicalSharing
    hardware:
        numCPU: 8
        memoryMB: 32768
        device:
        - *lsi_controller_0

The above YAML defines two SCSI controller instances for VMware.  These can then be used to define VM instances.  Underneath the controller definitions is a snippet from the definition of a VM using one of the controllers as one of the attached devices in the VM.  This would result in the following:


.. raw:: latex

    \newpage


.. code-block:: none

    {'controller0': {'key': 1000,
                     'sharedBus': 'vim.VirtualSCSISharing.noSharing',
                     'type': 'vim.vm.device.VirtualLsiLogicController'},
     'controller1': {'key': 1001,
                     'sharedBus': 'vim.VirtualSCSISharing.physicalSharing',
                     'type': 'vim.vm.device.VirtualLsiLogicController'},
     'hardware': {'device': [{'key': 1000,
                              'sharedBus': 'vim.VirtualSCSISharing.noSharing',
                              'type': 'vim.vm.device.VirtualLsiLogicController'}],
                  'memoryMB': 32768,
                  'numCPU': 8}}

You can see that the entire data structure defined by the ``lsi_controller_0`` is present in the list under ``hardware/device``.  If there were only one VM defined in the system then this wouldn't save anything, in fact it would increase the number of lines in the file.  However, when you have dozens of VMs defined, this can save a considerable amount of space in the files as well as ensures that the VMs are configured more consistently since they are all using the same definition for ``lsi_controller_0``.

In the above examples it showed two syntaxes for creating anchors:

#.  .. code-block:: none

        key_name: &anchor_name value

#.  .. code-block:: none

        key_name: &anchor_name
            subkeys: ...

There is a third (3) method. It is used to create a mapping out of a list:

.. code-block:: none

    - &anchor_name
      key_name: ...

or

.. code-block:: none

    - &anchor_name
      - item1
      - item2
      - ...

This last form creates a mapping anchor from a list item.  Functionally there is no difference between methods (2) and (3).  They both will resolve links to mappings.

.. important::

    There is no way of creating a anchor/link pair that provides a collection/list.  The following looks like it should create a long list, but instead it creates a list of lists:

.. code-block:: none

    - &list1
      - item1
      - item2
    - &list2
      - item3
      - item4

    - my_long_list
      - *list1
      - *list2

This actually results in the following structure:

.. code-block:: none

    - my_long_list
      - - item1
        - item2
      - - item3
        - item4

This is because it treats all links as values of a mapping key.  This means you CANNOT use links to concatenate collections/lists together.  If you needs lists that have significant overlap, you either need to duplicate the data in separate collections or programitically concatenate the lists after the YAML has been loaded.


.. raw:: latex

    \newpage
