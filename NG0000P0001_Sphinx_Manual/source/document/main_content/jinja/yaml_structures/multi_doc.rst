#######################################
Multi Document Support
#######################################

Per the YAML specification, only one file is loaded with YAML.  While this could have worked, it made it very difficult to edit the configuration of the system since the configuration file could be fifteen thousand lines long.  That would have been unmaintainable.  So the ``yaml_config`` library will load all of the :file:`.yml` files in the specified directory.  This allows the configuration of the VMs to be separate from the definitions of the VLANs, which is separate from the Windows Server configuration.  Now each of the configuration files can be specific to what it is specifying which makes it easier to make minor edits to the system.  It does add some complications since you may need to edit multiple files to add or remove items from the configuration of the system, but overall it seems to be a valid trade off.

The ``pyyaml`` parser only understands loading one file (or string) per the standard.  The ``yaml_config`` library takes all of the :file:`.yml` files in a given directory and concatenates them all together before handing the result to ``pyyaml``.  This can result in two issues:

#.  Per the YAML specification, if a key is redefined, then the entire data structure is redefined.
#.  If you recall from an earlier section, all anchors need to be defined before the links are used (``pyyaml`` issue).

The first issue shouldn't come up often since each file in the directory should be in its own namespace (this is described in the YAML schema for each release), but it could cause issues.

The second is a bigger issue because it means the files need to be concatenated in a specific order.  If they are loaded in the wrong order ``pyyaml`` will throw an error.  To resolve this issue the ``yaml_config`` library looks for ``#include filename`` statements each of the files it loads.  If it sees a line look like this, it will ensure that all of the included files are loaded prior the file that references them.  Take for example the include statements shown here::

    #include interfaces.yml
    #include vlans.yml
    #include dns.yml

This says ensure that those three (3) files found in the same directory as the referencing file are loaded prior to loading this file.  This can be used to ensure that all of the anchors defined in other files are loaded prior to the links where they are used.

.. note::

    The files referenced by the ``#include`` statements can be relative paths to other files.

Below is an example of a relative path to another file:

.. code-block:: none

    #include ../<domain>/hosts.yml


.. raw:: latex

    \newpage


In this path, the meanings of each section are as follows:

1. ``..`` ==> Up one directory.

2. ``<domain>`` ==> Arbitrary variable defined in variables.yml

3. ``hosts.yml`` ==> Arbitrary config file to load.

To check for this kind of error executer the exportConfiguration.py function mentioned earlier in the ``How to Load Simple YAML in Python`` section. This function executes the wrapper and will let you know about anchor/link errors.

To verify any changes, cd in Cygwin to the directory wanted (e.g. C:/ss_hardware/documents/DevOps/MD1.3.2/branches/M01.03.02.00.06/configuration_files/mcee/mcee), run python and add the following code:

.. code-block:: none

    >>> import yaml_config
    >>> ycp = yaml_config.yaml.config_parser('directory')
    >>> ycp.load_configuration()

The load_configuration function causes it to re-read all of the files, so once the shell is open, hitting the up arrow followed by enter will begin a reload and the changes can be seen.

To output data we use the pprint function. pprint = pretty print; you can name it whatever you like, like: from pprint import pprint as angela_printing_function from pprint import pprint.

Start with the below example, although it is likely more information than you want:

.. code-block:: none

    pprint(ycp.config)

To follow up, use List Comprehension (information from Peter). Then narrow down the list of hosts that you want to review in Python:

.. code-block:: none

    pcoip_hosts = [x for x in ycp.config['host'] if 'pcoip' in x.get('os', '') ]

Alternatively, if you want to do this with just the host name the following can be used:

.. code-block:: none

    pcoip_hosts = [ x['hostname'] for x in ycp.config['host'] if 'pcoip' in x.get('os', '') ]

Finally get you output using the following line:

.. code-block:: none

    pprint(pcoip_hosts)


.. raw:: latex

    \newpage
