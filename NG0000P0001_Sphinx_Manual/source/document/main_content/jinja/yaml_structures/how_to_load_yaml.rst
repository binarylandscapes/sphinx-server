########################################
How to Load Simple YAML in Python
########################################

In the next few sections we will be discussing YAML structure and how it maps to Python data structures.  For those of you who use Power Shell, there is a script that will convert the YAML to a :file:`.ps1` file that can be sourced.

In the simple case, you can load a short YAML configuration by pasting it in the REPL.  For example:

.. code-block:: python

    >>> import yaml
    >>> from pprint import pprint
    >>> y = yaml.load('''
    ... my_simple_yaml_file:
    ...     key: value
    ... ''')
    >>> pprint(y)
    {'my_simple_yaml_file': {'key': 'value'}}

If your YAML example is a bit longer and you want to store it in a file, you can read the file and load it with the following commands:

.. code-block:: python

    >>> y = yaml.load(open('your_file_here').read())

If you are using Power Shell, there are some extra things you need to be aware of:

#.  The yaml files must end with :file:`.yml`
#.  It will be using yaml_config to load all of :file:`.yml` files in the directory you specify.  We will cover what this means later.
#.  In each directory where there are :file:`.yml` files that you wish to load, you must also create a :file:`variables.yml` file with the following contents:

    .. code-block:: none

       variables:
           some_string_that_wont_be_used:


.. raw:: latex

    \newpage


Once you understand the above conditions, you can convert the YAML to a Power Shell data structure by running the following:

.. code-block:: none

    exportConfiguration.py \
        -t ps \
        -i 'your_directory_name_here' \
        -o 'your_output_file.ps1'

.. note::

    Cygwin uses unix slashes (/) not windows slashes (\\) to navigate through file paths.

If you already have your computer setup to build Sphinx Documents, this Python script should be installed to :file:`\usr\bin` and thus already in your path when using Cygwin.  If you want to call this from a command prompt you will need to run something like this:

.. code-block:: none

    C:\cygwin64\bin\python2.7 C:\cygwin64\bin\exportConfiguration.py ^
        -i 'your_directory_name_here' ^
        -o 'your_output_file.ps1'

########################################
How to Load Multiple YAML in Python
########################################

In the simple case, you can load a short YAML configuration by pasting it in the REPL.  Copy hosts.yml and hardware.yml to folder configuration_files For example:

.. code-block:: python

    >>> from yaml_config import yaml_config_parser as ycp
    >>> from pprint import pprint
    >>> y = ycp('configuration_files')
    >>> y.load_configuration()
    >>> c = y.config
    >>> print((c['hardware'][0]['ngDrawingInfo']['number'])+(c['hardware'][0]['ngDrawingInfo']['assy']))
    >>> [ pprint(h['ngDrawingInfo']) for h in c['hardware'] if h.get('ngDrawingInfo')  ]


.. raw:: latex

    \newpage
