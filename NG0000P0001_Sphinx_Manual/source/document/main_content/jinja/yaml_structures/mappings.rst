####################################
Mappings
####################################

In YAML, a mapping is a method of mapping 'keys' to 'values'.  A simple example is shown here:

.. code-block:: none

    key: value

The delimiter between the node and the value is a colon (:) followed by a single space.  Like a collection, when the YAML is loaded it will guess at the data type, take for example this YAML file:

.. code-block:: none

    string: this is a string
    integer: 1
    float: 1.0
    hex-number: 0x42
    hex-number-str: '0x42'
    octal-number: 042

This would produce the following python dictionary:

.. code-block:: python

    {'string': 'this is a string',
     'integer': 1,
     'float': 1.0,
     'hex-number': 66,
     'hex-number-str': '0x42',
     'octal-number': 34
    }

The above demonstrates the variety of mappings that the interpreter will use when loading the YAML.  For example, if it finds something that looks like a hex or octal number, it will be converted to an integer.  If you want it to stay a string, it needs to be quoted.

Mappings can be nested in one another, for example:

.. code-block:: none

    level_1_mapping:
        level_2_mapping: value2
        level_2_mapping2:
            level_3_mapping:
        level_2_mapping3:
    level_1_mapping2: ''

If a mapping has subkeys, then it cannot have a value.  Its value is the mappings it contains.  The names in the above example are descriptive for example purposes, the key names have no effect on the levels of the mappings, that is completely controlled by the indentation level.  Per the YAML specification any indentation that is greater than the previous line counts as a new level of mappings.  All mappings at the level MUST be indented the same amount.  For ease of use, please always use four (4) spaces for indentation.

The above example would produce the following data structure in Python:

.. code-block:: python

    {'level_1_mapping': {'level_2_mapping': 'value2',
                         'level_2_mapping2': {'level_3_mapping': None},
                         'level_2_mapping3': None},
     'level_1_mapping2': ''}

This example shows one additional artifact of using the ``pyyaml`` Python module.  If a mapping isn't given a value in the YAML, when it is loaded it will be given the Python value of ``None``.  When exported to power shell, it will be given the PS value of ``$null``.


.. raw:: latex

    \newpage
