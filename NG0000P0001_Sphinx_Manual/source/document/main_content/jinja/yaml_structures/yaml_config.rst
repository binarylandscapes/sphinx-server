##############################################
Custom YAML Loader Wrapper
##############################################

Behind the scenes, we are using the ``pyyaml`` library to load the YAML configuration files.  However we use the NGC developed ``yaml_config`` library (part of the Sphinx installation) which wraps ``pyyaml`` and adds a few features.  They are described below:

.. toctree::

    multi_doc.rst
    variables
    