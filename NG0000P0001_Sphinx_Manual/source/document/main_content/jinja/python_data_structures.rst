#########################################
Python Basic Data Structures (Overview)
#########################################

In Python there are a few basic data structures that need to be well understood in order successfully use Sphinx with Jinja. Open Cygwin, type python to open the program and practice along with the examples:

.. toctree::

    python_data_structures/list
    python_data_structures/string
    python_data_structures/tuple
    python_data_structures/dict

.. raw:: latex

    \newpage
