########################################
YAML (Overview)
########################################

YAML, depending on which site you look at for a definition stands for one of the following:

* YAML Ain't a Markup Language
* Yet Another Markup Language

Either way it is a humanly readable method of storing structured data.  We use this to store all of the system configuration data that will drive the documentation.  Python and YAML work well together which you will see shortly.


General YAML Rules
The basic rules of YAML are:

*   Never use tabs
*   Indentations must use spaces, by convention, use four (4) spaces
*   The first unquoted '``#``' symbol denotes the rest of the line is a comment

.. toctree::

    yaml_structures/how_to_load_yaml
    yaml_structures/collections
    yaml_structures/mappings
    yaml_structures/nesting
    yaml_structures/anchors_and_links
    yaml_structures/yaml_merge_operator
    yaml_structures/yaml_config
    yaml_structures/custom_data_types

.. raw:: latex

    \newpage
