#######################################
list
#######################################

The first data stucture is a ``list``.  This data structure is usually the most easily understood data structure because many people use lists every day.  It is simply an ordered collection of objects.  A ``list`` is a ``sequence`` type, while not important now, knowing this up front may help some of the concepts make more sense later.

A list is defined by a comma separated list of objects surrounded by square brackets.  For example:

.. code-block:: python
    :caption: My first list

    >>> my_list = [1, 1, 2,  3,
    ...            5, 8, 13, 21,]

.. note::

    ... is a continuation indicator from the Python program. Do not copy/paste this from the examples. Python employs this signal when it is waiting for the end operator, e.g. ``]`` if a data set was started with ``[``.

For those of you who know a little about math, you may recognize this as the start of the Fibonacci sequence.  In addition to this fun fact, there are a couple of other things to notice:

*   A list must start and end with square brackets (``[`` and ``]``)
*   There are 8 objects (``int``\ s in this case)
*   Each object is separated by a comma (``,``)
*   Python is smart enough to know that the list doesn't end when it gets to a line break, it looks for the closing bracket.  This can make code more readable.
*   This list has the *optional* trailing comma.

This is nice, but we need to know how to get the data out list.  First we need to understand that lists are zero indexed.  This means that the first element in the list is at position ``0`` and the last element is always at ``n-1`` where ``n`` is the number of the items in the list.  How does this work in practice?

.. code-block:: python
    :caption: Lists are zero-indexed

    >>> my_list[0]
    1
    >>> my_list[7]
    21


.. raw:: latex

    \newpage


Lists can also be negatively indexed, that is to say that is ``-1`` is the last element in the list, and in our example, ``-8`` would be the first element in the list.

.. code-block:: python
    :caption: Lists can be negatively indexed

    >>> my_list[-1]
    21
    >>> my_list[-4]
    5

Sometimes you need to get a range of values out of a list, that can be done using the slice operator (``:``).  Using the slice operator you can get a new list (a shallow copy) or you can update elements of the list.

.. code-block:: python
    :caption: Introduction to the slice operator on lists

    >>> my_list[:]              # If you don't specify the start or stop, you get the whole list!
    [1, 1, 2, 3, 5, 8, 13, 21]
    >>> my_list[2:5]            # Get elements [2 through 5), the first number is inclusive, the second is exclusive
    [2, 3, 5]
    >>> my_list[2:5] = []       # Remove those three elements
    >>> my_list
    [1, 1, 8, 13, 21]
    >>> my_list[-3:]            # Get the last 3 elements in the list
    [8, 13, 21]

Now that you can get values out of a list, it would probably be good to know how to get information about the list.  The first thing you may need to know about a list is how many elements are in it.  Since a :class:`list` is a :class:`sequence` you can use the :func:`len` function.

.. code-block:: python
    :caption: Getting the length of a list

    >>> len(my_list)    # Remember we removed 3 elements?
    5

Do you need to combine two lists together?  No problem!  Here you can see that lists can contain different object types:

.. code-block:: python
    :caption: Concatenating lists

    >>> ['a', 'b', 'c', 'd'] + [5, 8, 13, 21]   # The + operator can be used to concatenate lists
    ['a', 'b', 'c', 'd', 5, 8, 13, 21]


.. raw:: latex

    \newpage


Lists can store other lists.  Each nested list is accessed just like any element in the original list, so you may need to specify multiple index levels.

.. code-block:: python
    :caption: Multilevels lists

    >>> multilevel_list = [ ['a', 'b', 'c'],
    ...                     [1, 2, 3, 4], ]
    >>> multilevel_list[0]
    ['a', 'b', 'c']
    >>> multilevel_list[0][-2]
    'b'
    >>> multilevel_list[1][0]
    1
    >>> len(multilevel_list)
    2
    >>> len(multilevel_list[-1])
    4

.. This part was stolen from the Python docs:

The list data type has some more methods.  Here are all of the methods of list
objects:


.. method:: list.append(x)
   :noindex:

   Add an item to the end of the list; equivalent to ``a[len(a):] = [x]``.


.. method:: list.extend(L)
   :noindex:

   Extend the list by appending all the items in the given list; equivalent to
   ``a[len(a):] = L``.


.. method:: list.insert(i, x)
   :noindex:

   Insert an item at a given position.  The first argument is the index of the
   element before which to insert, so ``a.insert(0, x)`` inserts at the front of
   the list, and ``a.insert(len(a), x)`` is equivalent to ``a.append(x)``.


.. method:: list.remove(x)
   :noindex:

   Remove the first item from the list whose value is *x*. It is an error if there
   is no such item.


.. method:: list.pop([i])
   :noindex:

   Remove the item at the given position in the list, and return it.  If no index
   is specified, ``a.pop()`` removes and returns the last item in the list.  (The
   square brackets around the *i* in the method signature denote that the parameter
   is optional, not that you should type square brackets at that position.  You
   will see this notation frequently in the Python Library Reference.)


.. method:: list.index(x)
   :noindex:

   Return the index in the list of the first item whose value is *x*. It is an
   error if there is no such item.


.. method:: list.count(x)
   :noindex:

   Return the number of times *x* appears in the list.


.. method:: list.sort(cmp=None, key=None, reverse=False)
   :noindex:

   Sort the items of the list in place (the arguments can be used for sort
   customization, see :func:`sorted` for their explanation).


.. method:: list.reverse()
   :noindex:

   Reverse the elements of the list, in place.


.. raw:: latex

    \newpage


An example that uses most of the list methods:

.. code-block:: python
    :caption: Other list functions

    >>> a = [66.25, 333, 333, 1, 1234.5]
    >>> print a.count(333), a.count(66.25), a.count('x')
    2 1 0
    >>> a.insert(2, -1)
    >>> a.append(333)
    >>> a
    [66.25, 333, -1, 333, 1, 1234.5, 333]
    >>> a.index(333)
    1
    >>> a.remove(333)
    >>> a
    [66.25, -1, 333, 1, 1234.5, 333]
    >>> a.reverse()
    >>> a
    [333, 1234.5, 1, 333, -1, 66.25]
    >>> a.sort()
    >>> a
    [-1, 1, 66.25, 333, 333, 1234.5]
    >>> a.pop()
    1234.5
    >>> a
    [-1, 1, 66.25, 333, 333]

You might have noticed that methods like ``insert``, ``remove`` or ``sort`` that
only modify the list have no return value printed -- they return the default
``None``.


.. seealso:: https://docs.python.org/2.7/tutorial/introduction.html#lists
.. seealso:: https://docs.python.org/2.7/tutorial/datastructures.html


.. raw:: latex

    \newpage
