####################################
Strings
####################################

Strings are a way to store an immutable sequence of characters.  The can be declared in 4 ways:

*   Using double quotes ``"some string"``
*   Using single quotes ``'some string'``
*   Using triple double quotes ``"""some string"""``
*   Using triple single quotes ``'''some string'''``

While this might seem confusing at first, it isn't that hard I promise.  The only difference between using single quotes and double quotes to denote a string is which quote character you need to escape if you need quotes in a string.  That is to say if you use single quotes to define the string, if you need a single quote in the string, you need to escape the single quote.  Same goes for double quotes.  If you need a single quote in a double quoted string there is no extra work.  Here are some examples:

.. code-block:: python

    >>> 'spam eggs'  # single quotes
    'spam eggs'
    >>> 'doesn\'t'  # use \' to escape the single quote...
    "doesn't"
    >>> "doesn't"  # ...or use double quotes instead
    "doesn't"
    >>> '"Yes," he said.'
    '"Yes," he said.'
    >>> "\"Yes,\" he said."
    '"Yes," he said.'
    >>> '"Isn\'t," she said.'
    '"Isn\'t," she said.'

When not using the triple quoted varieties the sting cannot be multiple lines, eg it can't be more than one line in you text editor.  You can however put an escaped newline (``\n``) in it to make the string look like it has multiple lines:

.. code-block:: python
    :caption: An escaped newline

    >>> s = 'First line.\nSecond line.'  # \n means newline
    >>> s  # without print, \n is included in the output
    'First line.\nSecond line.'
    >>> print s  # with print, \n produces a new line
    First line.
    Second line.


.. raw:: latex

    \newpage


If you want to have multiple lines without having to make it look like one really long string in your source code, you need to use the triple quoted variety.  The same rules for escaping quotes exist here.  If you need to use triple quotes in your string, you need to use the opposite type in your string or escape them.  If you include a slash (``\``) after the triple quotes it will skip the new lines on the first and last line for you.

.. code-block:: python
    :caption: A triple quoted string

    >>> print """\
    ... Usage: thingy [OPTIONS]
    ...      -h                        Display this usage message
    ...      -H hostname               Hostname to connect to
    ... """

    Usage: thingy [OPTIONS]
         -h                        Display this usage message
         -H hostname               Hostname to connect to

Strings can be repeated (multiplied) and concatinated (added), for example:

.. code-block:: python

    >>> 'Hello ' + 'Bob!'
    'Hello Bob!'
    >>> name = 'Bob'
    >>> print('Hello ' + name + '!')
    'Hello Bob!'
    >>> '!' * 3
    '!!!'
    >>> print('Hello ' + name + '!' * 3)
    'Hello Bob!!!'

Remember how I said that strings are like a list of characters?  That is because they can be indexed just like lists:

.. code-block:: python

    >>> word = 'Python'
    >>> word[0]  # character in position 0
    'P'
    >>> word[5]  # character in position 5
    'n'
    >>> word[-1]  # last character
    'n'
    >>> word[-2]  # second-last character
    'o'
    >>> word[-6]
    'P'
    >>> word[0:2]  # characters from position 0 (included) to 2 (excluded)
    'Py'
    >>> word[2:5]  # characters from position 2 (included) to 5 (excluded)
    'tho'
    >>> word[:2] + word[2:]
    'Python'
    >>> word[:4] + word[4:]
    'Python'
    >> len(word)
    6


.. raw:: latex

    \newpage


One thing you can't do with a string however, is change a letter at a specific location.  This is because they are immutable:

.. code-block:: python

    >> word[0] = 'J'
      ...
    TypeError: 'str' object does not support item assignment
    >>> word[2:] = 'py'
      ...
    TypeError: 'str' object does not support item assignment

If you need to change a string, you need to make a new one, for example:

.. code-block:: python

    >>> 'J' + word[1:]
    'Jython'
    >>> word[:2] + 'py'
    'Pypy'

If you find yourself escaping a lot in strings (probably because you are working on Windows), you may need to use raw string.  You can do this by putting an ``r`` just before the quotes.  This means ignore all escape characters and treat them like a regular character.  For example:

.. code-block:: python

    >>> print 'C:\some\name'  # here \n means newline!
    C:\some
    ame
    >>> print r'C:\some\name'  # note the r before the quote
    C:\some\name


.. seealso:: https://docs.python.org/2.7/tutorial/introduction.html#strings


.. raw:: latex

    \newpage
