***************************************
tuple
***************************************

The next data structure is a ``tuple``.  The easiest way to explain this is that it is an immutable set of objects.  The Python syntax for this is to use parenthesis to create a comma separated list of objects.  For example:

.. code-block:: python
    :caption: A first tuple

    my_tuple = (1, 1, 2, 3, 5)

Just like the ``list`` object, you can access the any of the variables using the index operator (brackets, ``[]``).  Remember that in most programming languages, one of which is Python, sets and lists are zero indexed.  So to access the first item in the ``tuple``, you would use ``my_tuple[0]`` while the last element of the tuple could be accessed using ``my_tuple[4]`` or ``my_tuple[-1]``.

A once defined, you cannot update the values in a ``tuple`` because it is a read only object, just like you can't change characters in a ``str`` object.  We won't go too much into more detail on a ``tuple`` because they act pretty much act like a list in other respects.  They are typically used when returning multiple values from a function.  You won't see this very often in Jinja, except for ``dict.iteritems()``, which we will get to in the next section.
