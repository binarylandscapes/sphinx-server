####################################
dict
####################################

If you have any other programming experience you may know the ``dict`` object as a dictionary, hash table, or hash map.  It is like a list, but the instead of indexing by number, you index by a key.  Here is a simple definition of a ``dict``::

    >>> numbers = {'one': 1,
    ...            'two': 2,
    ...            'three': 3,
    ...            'four': 4, }
    >>> numbers['two']
    2
    >>> len(numbers)
    4

You can see from the above example that a dictionary provides a "key" to "value" mapping, in this case, it is a mapping of the word representing a number to the numerical value.  This is a very simple example, so lets look at something more complicated::

    >>> server = {'hostname': 'appserver01',
    ...           'ip_address': '192.168.0.21',
    ...           'services': ['app1', 'app2', 'app3'],}
    >>> server['hostname']
    appserver01
    >>> server['services']
    ['app1', 'app2', 'app3']
    >>> server['services'][2]
    'app3'

As you can see from the above, you can use a ``dict`` object to keep track of attributes of an object.  You can add any type of object as the value for any key.  You can also use any object that can be converted to a string for the key as well.  In the above example, we assigned a list of ``services`` that run on ``appserver01``.  Nesting ``dict``\ s in ``list``\ s or ``list``\s in ``dict``\ s is a very common thing to do and is used extensively when working with YAML as you will see in a little while.


.. raw:: latex

    \newpage
