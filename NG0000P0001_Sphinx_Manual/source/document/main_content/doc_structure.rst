=================================================
Sphinx Documentation Structure
=================================================

From an official Sphinx-doc.org documentation standpoint, the applicable folder structure starts at :file:`/source` and is perfectly fine to use as the basis while learning. From a production standpoint, the folder structure is more robust to include aspects for configuration management using tools like :program:`Subversion` or :program:`Git`.

.. note::

    You can copy the :file:`/sphinx/baseline_sphinx_env/` folder and all of its contents to your own location to edit from :media:`SPHINXDVD0001` disc.

.. note::

    It is possible change this structure to match each programs needs but your :file:`conf.py` will have to be updated to property scan your folder structure to create :samp:`{ version }` and :samp:`{ release }` properly as they are used in the sphinx document creation and file names.

.. note::

    Currently in the included template :file:`conf.py`, you must have a letter or string in front of :samp:`{ majorVersion }.{ minorVersion }` and :samp:`{ majorVersion }.{ minorVersion }.{ buildVersion }` so if your program does not have one then just use :kbd:`IFC`. Example is this procedure uses :kbd:`SPHINX`

.. note::

    Ensure in all files and folders of your document there is no special characters or spaces in the names. You are using Cygwin as your environment and thus using a UNIX/Linux based file structure. That is why all folders are defined as using the back slash ``/`` instead of the forward slash ``\`` used in Windows.

.. note::

    Ensure none of your files you create start with :kbd:`_` and none are named **genindex, modindex, search** as those have reservation within Sphinx.


.. raw:: latex

    \newpage


The NG Sphinx folder structure is (starting at top level):

.. note::

    This description is for a Git based Sphinx repository as the Version and Release are manually set in :file:`conf.py` and the branch of Git repository is auto-discovered during build and added to footer. Other projects may use SVN and have their ``release`` and ``version`` discovered during build via additional folder structures not listed. That discovery scripting is still present in :file:`conf.py` though for commonality.

#.  :file:`baseline_sphinx_env/` - is your associated description for your program, that is related to the documentation under this folder. Through python scripts in :file:`conf.py` and Jinja2 this is the :samp:`{ version }` variable. Files outside of this folder will not share the same common Sphinx settings as other folders like for example different latex styles and cover page material for different programs or Unclassified versus Unclassified//For Official Use Only documents.

    #.  :file:`common/` - is where all of your common Sphinx related files reside for this particular release. Such as appendices, jinja macros, media.yaml and latex_templates. A regular user of Sphinx should only be editing the :file:`common/appendices/acronym_list.rst` and adding needed appendice files to that location or editing :file:`media.yaml`
    #.  :file:`configuration_files/` - this is where you store all of the yaml files for Sphinx to pull from for different builds. This will also have its own folder structure to match with :file:`conf.py` Jinja contexts needs but is covered more in Jinja section of manual, as none are needed to start using Sphinx.
    #.  :file:`scripts/` - this is where you store all of the script files for Sphinx to pull from to document and also utilize in system so your documentation is CM matched with code or scripts.
    #.  :file:`template_document/` - This is your template document for your program and is the baseline for all users to copy/clone from when starting a new document.
    #.  :file:`new_document_number_from_CM/` - This is a single document you have created and requested a document number from CM to use as the folder name. You will have multiple of these folders. This can be just a descriptive name of procedure if that procedure will be linked to other higher level system procedures later using links and Jinja.
    #.  :file:`make_links.py` - This is the python script called from make_links.sh that makes the soft links defined in your document :file:`links` file.
    #.  :file:`make_links.sh` - This is the bash shell script ran from Cygwin to start the build of your document in the desired output.

-----------------------------------
Simple Document Folder Structure
-----------------------------------
This folder structure is described from contents of the :file:`new_document_number_from_CM/` folder in the previous section.

:file:`source/index.rst` - has a Table of Contents built link to

:file:`document/main_content/contents.rst` and load all rst files from :file:`document/appendices/*`

:file:`source/document/main_content/contents.rst` - Table of Contents "Heading 1", place all.rst files here in :file:`source/document/main_content/`. and update :file:`contents.rst` located in that folder.


.. raw:: latex

    \newpage


--------------------------------------
Multi-Layer Document Folder Struction
--------------------------------------
This folder structure is described from contents of the :file:`new_document_number_from_CM/` folder in the previous section.

.. note::

    You are not required to create a file for ``:toctable:`` for each folder you create. The requirement is the main one in :file:`source/document/main_content` that loads every file you desired to be rendered. You could still organize your rst files into multiple levels of folders as long as the path in the ``:toctable:`` is correct.

Example: Procedure with the following folder structure building off the previous Simple Document section configuring a Network Switch and a Rack KVM:

:file:`source/document/main_content/contents.rst` - Main Table of Contents "Heading 1", listing of the location of the (2) lower level contents files in :file:`rack_kvm/` and :file:`network_switch/`

:file:`source/document/main_content/rack_kvm/contents.rst` - Subfolder Table of Contents "Heading 2", has listing of all related subfolders with glob /* or just place .rst files (as needed).

:file:`source/document/main_content/rack_kvm/kvm_configuration.rst`

:file:`source/document/main_content/network_switch/contents.rst` - Subfolder Table of Contents "Heading 2", has listing of all related subfolders with glob /* or just place .rst files (as needed).

:file:`source/document/main_content/network_switch/connect_to_switch/` - Place .rst and any other needed files for .rst file

:file:`source/document/main_content/network_switch/connect_to_switch/connect_to_switch.rst` - RST file rendered by being in the ``:toctable:`` of :file:`../network_switch/contents.rst`

:file:`source/document/main_content/network_switch/connect_to_switch/putty_serial_menu.png` - Image file to insert into document from rst file.

:file:`source/document/main_content/network_switch/connect_to_switch/putty_session_serial_com1.png` -Image file to insert into document from rst file.


.. raw:: latex

    \newpage


------------------------
Creating Appendices
------------------------

For a repeatable procedure that you may use multiple times in your document it makes more sense to create your own local appendice for your document that people can still load into their own with a link (if needed):

#.  Create the rst files in :file:`source/document/appendices/`
#.  Do only **one** of the following things:

    #.  Verify or change in your :file:`source/index.rst` that your Table of Appendices ``:toctree:`` has a glob and the :file:`document/appendices/*` directory listed to load any rst files in that folder.
    #.  Verify or change in your :file:`source/index.rst` that your Table of Appendices ``:toctree:`` has the :file:`document/appendices/contents` directory listed and you have a contents.rst file in that location configured to load your desired appendices files.
    #.  Verify or change in your :file:`source/index.rst` that your Table of Appendices ``:toctree:`` has a path to each :file:`document/appendices/{rst file}` you wish to load. This includes if you linked to other document appendices to :file:`source/common/` via links.

#.  Ensure that each of you appendix rst files has a reference link as described in :ref:`Link to a reST Anchor <rst_ref_linking>`
#.  In actual rst document section where you want to link to those sections, to jump to an appendix, use the following:

    .. code-block:: none

        :ref:`Appendix Title or other link name <reference_link_label>`


.. raw:: latex

    \newpage


=====================================
Building a Table of Contents
=====================================

.. _building_toctable:

This is the primary method used to link files together into a larger document. (Table of Contents)

.. note::

    While if using globs in your table of contents, you can start the files and folder with ## to order automatically but it is highly recommended to use a more detailed content.rst file with the toc to control ordering of creation

*   ``:maxdepth:`` - A numeric maxdepth option may be given to indicate the depth of the tree; by default, all levels are included.
*   ``:numbered:`` - Adds section numbers even in HTML output. Can also be limited by giving a numeric argument.
*   ``:name:`` - Implicit target name that ``:ref:`` can use.
*   ``:caption:`` - Provides a toctree caption
*   ``:titlesonly:`` - Shows only the titles of documents in the tree not other headings of same level
*   ``:glob:`` - All entries are matches against list of availble documents and then ordered into a alphabetical list. Required for ``folder/*`` entries.
*   ``:reversed:`` - Reverses ordering of list entries
*   ``:hidden:`` - Will notify Sphinx of document heirarchy but not build links. Useful if making custom links yourself
*   ``:includehidden:`` - If you want to have top level toctree linked but all other toctrees hidden.

.. note::

    Include ``:orphan:`` at top of rst file if it will not be part of toctree and to clear build warning.

.. code-block:: none

    .. toctree::
        :maxdepth: 4
        :numbered:
        :name: mastertoc
        :caption: Table of Contents
        :glob:

        rst_file01
        folder01/rst_file02
        folder02/*

.. raw:: latex

    \newpage
