#####################################################
Sphinx Manual
#####################################################

.. toctree::
    :maxdepth: 5
    :glob:

    enviro_setup
    doc_structure
    syntax/contents
    jinja/contents
    getting_started
