=================================================
Sphinx Environment Setup
=================================================

To get started, you will need to install your Sphinx development environment. Due to limitations and difficulty of Windows and NGMN security, Cygwin is the best method to accomplish this that is approved by the NGMN Enterprise Software List and does not required Administrator rights.

#.  Either burn :media:`SPHINXDVD0001` to a DVD-R or mount the :media:`SPHINXDVD0001` ISO file to your NGGN machine.
#.  Browse and execute the :file:`install.bat` from top level directory. This will run through an offline preconfigured install of :program:`Cygwin64` and our required packages to :file:`C:\\cygwin64\\`. Just follow along in the prompts, making sure you don't get any errors like "pkg failed SHA".
#.  Once Cygwin installer is complete, run :program:`Cygwin64` from your new desktop icon. Leave media installed or ISO file mounted for next steps.
#.  Now run the following command in :program:`Cygwin` console. This will run through the Cygwin environment setup and offline installer for Python packages and other Cygwin packages to be used in your Sphinx Environment.

    .. important::

        While Sphinx itself is pretty robust and upgrading packages usually has no ill effects. The Latex packages and configuration utlized is very sensitive and thus the installer is locked down to certain versions and is based off the Cygwin packages from the :media:`SPHINXDVD0001` disc. Any changes from that may have adverse impacts on your environment. If you find a interesting extention or package for Sphinx, reach out to your environment administration to request it, so it can be properly integrated and tested.

    .. note::

        The command is using the "D" drive but replace this letter of the drive where media or ISO file is mounted.

    #.  :kbd:`cd /cygdrive/d/sphinx`
    #.  :kbd:`./install.sh`

#.  Once complete with the Sphinx installation, run the following command to verify your environment is ready. It will use Sphinx to build this exact document in livehtml and latexpdf, demonstrating your computer is properly setup. You will have to refresh the opened default browser due to timing.

    #.  :kbd:`./test.sh`

#.  If you got a Sphinx Manual in PDF and via HTML, congrats! you are ready to start documenting in RestructredText. Continue on in the procedure to a whole new way of documentation!

.. note::

    It is helpful to make softlinks in :program:`Cygwin` to your Sphinx folders rather than continually use :kbd:`/cygdrive/c/.....` The command for this is :samp:`ln -s /cygdrive/c/{local folder path} /{desired folder path from Cygwin root}`

.. raw:: latex

    \newpage
