import re
import subprocess
import os
from pprint import pprint
import git

useSvn = False
useGit = False
dirty_build = False
gitstatus = 'not'

def is_in_git(path):
    try:
        output = subprocess.check_output('git status ' + path,
                                         shell=True,
                                         stderr=subprocess.STDOUT,)

        if 'but untracked files present' in output:
            dirty_build = True
            return True
        if 'deleted:' in output:
            dirty_build = True
            return True
        if 'modified:' in output:
            dirty_build = True
            return True
    except subprocess.CalledProcessError as e:
        return False
    return True

def dirty_build_git(path):
    try:
        output = subprocess.check_output('git status ' + path,
                                         shell=True,
                                         stderr=subprocess.STDOUT,)

        if 'but untracked files present' in output:
            return True
        if 'deleted:' in output:
            return True
        if 'modified:' in output:
            return True
        if 'nothing to commit, working tree clean' in output:
            return False
    except subprocess.CalledProcessError as e:
        return True

def get_git_status():
    '''Gets the Git status of the build directories

    This function opens the ``links`` file to determine what directories are
    included in this project.  It gets a detailed Git report of all of the files.
    It returns the highest Git revision found in all of the files and
    directories present.  In addition, if there are any local modifications that
    have not been checked into Git, it will append the string ``Dirty Build``
    to the version string.

    Args:
        None

    Returns:
        str: A string consisting of the Git version (int) with the potential of
             a string ``- (dirty) -`` being appended if local modifications are
             present.
    '''
    if not is_in_git('.'):
        return [-1, 'Not in CM']
    git_repo = git.Repo(search_parent_directories=True)
    git_head = git_repo.heads[0]
    git_head_name = git_head.name
    git_sha_full = git_repo.head.commit.hexsha
    git_sha = git_sha_full[:10]
    return ' '.join(['Git:',git_head_name,'- (dirty) -' if dirty_build_git('.') else '-',git_sha])

def is_in_svn(path):
    try:
        output = subprocess.check_output('svn pg svn:ignore ' + path,
                                         shell=True,
                                         stderr=subprocess.STDOUT,)

        if 'is not a working copy' in output:
            return False
    except subprocess.CalledProcessError as e:
        return False
    return True

def get_svn_status():
    '''Gets the SVN status of the build directories

    This function opens the ``links`` file to determine what directories are
    included in this project.  It gets a detailed SVN report of all of the files.
    It returns the highest SVN revision found in all of the files and
    directories present.  In addition, if there are any local modifications that
    have not been checked into SVN, it will append the string ``Dirty Build``
    to the version string.

    Args:
        None

    Returns:
        str: A string consisting of the SVN version (int) with the potential of
             a string ``Dirty Build`` being appended if local modifications are
             present.
    '''
    if not is_in_svn('.'):
        return [-1, 'Not in CM']
    # Get the listing of directories where the source files exist
    previous_dir = ''.join([os.pardir, os.sep])
    directories = subprocess.check_output(["cat {}/links | awk '{{print $2}}'".format(previous_dir)],
                                          shell=True).split()
    directories = [os.pardir] + [''.join([previous_dir, dir]) for dir in directories]
    # Get the status of all of the files
    svn_status = subprocess.check_output(['svn status -v {}'.format(' '.join(directories))], shell=True)
    dirty_build = False
    svn_versions = ''
    working_versions = []
    dirty_files = []
    for line in svn_status.split('\n'):
        # skip blank lines
        if not line:
            continue
        # The flags are the first 9 columns
        svn_flags = line[0:9]
        variable_fields = line[10:].split()
        if '?' in svn_flags:    # Non-versioned file
            filename = variable_fields[0]
        elif '>' in svn_flags:  # File was moved
            filename = variable_fields[2]
        else:
            working_version = variable_fields[0]
            working_versions.append(working_version)
            commited_version = variable_fields[1]
            filename = variable_fields[3]
        # The flags are the first 9 columns
        if svn_flags != ' '*9:
            dirty_files.append(' '.join([svn_flags, filename]))
    # Convert the full set to a list unique values
    working_versions = list(set(working_versions))
    if dirty_files or len(working_versions) != 1:
        dirty_build = True
        print('*****************************************************')
        if len(working_versions) != 1:
            print('The build is marked dirty due the files being at different SVN working versions.  Please perform a full update.')
            print('The build contains {} different versions which are {}'.format(len(working_versions), working_version))
            svn_versions = min(working_versions) + '-' + max(working_versions)
        else:
            svn_versions = working_versions[0]
        if dirty_files:
            print('The build is marked dirty due to the following files:')
            print('\n'.join(dirty_files))
        print('*****************************************************')
    else:
        svn_versions = working_versions[0]
    print('Using SVN Version(s): ' + svn_versions)
    return ' '.join([svn_versions, 'Dirty Build' if dirty_build else ''])

def iter_replace(input_string, replacement_dict):
    '''Performs a regex replace

    Performs all regex replacements found in ``replacement_dict`` on
    ``input_string``.  The keys in ``replacement_dict`` are the search regexes
    and the values are the replacement values.

    Args:
        input_string (str) : The text to perform the replacement function on

        replacement_dict (dict) : A ``dict`` containing the keys (search
                    expressions) and values (replacement text).

    Returns:
        str : The ``input_string`` with the replacement defined in
                    ``replacement_dict``.
    '''
    rep = dict((re.escape(k), v) for k, v in replacement_dict.iteritems())
    pattern = re.compile('|'.join(rep.keys()))
    return pattern.sub(lambda m: rep[re.escape(m.group(0))], input_string)

def escape_latex(text):
    '''escapes special characters in LaTeX

    Args:
        text (str) : The text to escape

    Returns:
        str: The escaped text
    '''
    replacements = {'&': r'\&',
                    '\\': r'\textbackslash',
                    '\n': r'\\',
                    '%': r'\%',
                    '$': r'\$',
                    '#': r'\#',
                    '_': r'\_',
                    '{': r'\{',
                    '}': r'\}',
                    '~': r'\textasciitilde',
                    '^': r'\textasciicircum',
                   }
    return iter_replace(str(text), replacements)

def wrap_table_cell(text, align='tl'):
    '''escapes and wraps data to be in a compatible format for LaTex

    The specified alignment only occurs if the text contains newlines ('\n').
    Multiline text is wrapped with ``\makecell`` and escaped.  If only a single
    line is passed, then it just escaped.

    Args:
        text (str) : A str with the cell contents

        align (str): A str containing the alignment requirements for the cell.
                     All values are option, defaults to 'tl'
                     Vertical alignment options are:
                        * ``t`` for top
                        * ``m`` for middle
                        * ``b`` for bottom
                     Horizontal alignment options are:
                        * ``l`` for left
                        * ``c`` for center
                        * ``r`` for right

    Returns:
        str : The wrapped cell contents properly escaped.
    '''
    # Handle None from yaml
    if text is None:
        return ''
    wrap_cell = False
    if '\n' in str(text):
        wrap_cell = True
    text = escape_latex(text)
    if wrap_cell:
        return ''.join(['\makecell[', align, ']{', text, '}'])
    return text

def array_to_latex_table(rows, header=False, grid=False):
    '''creates a LaTeX table from list or a list of lists

    This creates the "guts" of a LaTeX table from a list (single row) or a list
    of lists (table).  This DOES NOT create any ``table`` environments, captions,
    or anything else, this just takes the information in the lists and creates
    a ``str`` to represent the cells.

    Args:
        rows (list) : A ``list`` or ``list`` of ``lists``.  Each item is the
                      contents of a single table cell.

        header (bool) : If ``True``, it will make the cell contents bold.

        grid (bool) : If ``True`` it will append a horizontal line after each
                      row.

    Returns:
        str : A string with valid LaTeX table contents (no table env).
    '''
    latex_row = ''
    latex_rows = []
    if rows:
        if not isinstance(rows[0], list):
            rows = [rows]
    for row in rows:
        try:
            if header:
                latex_row = ' & '.join([''.join(['\\makecell{\\textbf{', escape_latex_cell(col), '}}']) for col in row])
            else:
                latex_row = ' & '.join([wrap_table_cell(col) for col in row])
            if grid:
                latex_row = ' '.join([latex_row, r' \\ \hline'])
            else:
                latex_row = ' '.join([latex_row, r' \\'])
        except:
            print '\nproblematic row: \n{}\n'.format(row)
            raise
        latex_rows = latex_rows + [latex_row]
    return '\n'.join(latex_rows)
