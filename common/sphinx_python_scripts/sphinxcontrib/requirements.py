import os
from pprint import pprint
import time

from docutils import nodes
from docutils.parsers.rst.directives.tables import Table
from docutils.parsers.rst import directives
from docutils.parsers.rst import Directive

import yaml

def setup(app):
    app.add_config_value('include_requirements', True, 'env')
    app.add_config_value('required_media_list', 'common/media.yaml', 'env')
    app.add_node(required_media_list,
                 html=(visit_pass, depart_pass),
                 latex=(visit_pass, depart_pass),
                 text=(visit_pass, depart_pass))
    app.add_directive('required_media_list', RequiredMediaListDirective)
    app.add_role('media', media_role)
    app.connect('doctree-resolved', process_required_media_list_nodes)
    app.connect('builder-inited', builder_inited)
    app.connect('env-purge-doc', env_purge_doc)
    app.connect('env-get-outdated', env_get_outdated)


    app.add_config_value('required_equipment_list', 'common/equipment.yaml', 'env')
    app.add_node(required_equipment_list,
                 html=(visit_pass, depart_pass),
                 latex=(visit_pass, depart_pass),
                 text=(visit_pass, depart_pass))
    app.add_directive('required_equipment_list', RequiredEquipmentListDirective)
    app.add_role('equipment', equipment_role)
    app.connect('doctree-resolved', process_required_equipment_list_nodes)


    return {'version': '0.1'}

def visit_pass(self, node):
    pass


def depart_pass(self, node):
    pass

def env_purge_doc(app, env, docname):
    '''Removes the cached information about `docname`'''
    env.required_media.pop(docname, None)


def env_get_outdated(app, env, added, changed, removed):
    '''Determines the list of files that use media and need to be rerendered

    To determine if a file has "changed", it needs to evaluate the following
    items:

    * Has the database been updated since the last run?
    '''

    if env.media_current_timestamp != env.media_last_timestamp:
        env.media_last_timestamp = env.media_current_timestamp
        return env.required_media.keys()
    else:
        return []


def builder_inited(app):
    '''Initializes the environment for this extension

    This will load the media list into the env and create the place holder for
    storing the document/media mapping'''
    env = app.builder.env
    if not hasattr(env, 'media_last_timestamp'):
        env.media_last_timestamp = 0
    fname = os.path.join(app.confdir, app.config.required_media_list)
    env.media_current_timestamp = os.path.getmtime(fname)
    if env.media_current_timestamp != env.media_last_timestamp:
        print('Loading ' + fname + ' to get update media content.')
        if not hasattr(env, 'media_titles'):
            env.media_titles = {}
        with open(fname, 'r') as f:
            env.media_list = yaml.load(f.read())
    if not hasattr(env, 'required_media'):
        env.required_media = {}


def media_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    """Creates a media role

    Returns 2 part tuple containing list of nodes to insert into the
    document and a list of system messages.  Both are allowed to be
    empty.

    Args:
        name (str): The role name used in the document.

        rawtext (str): The entire markup snippet, with role.

        text (str): The text marked with the role.

        lineno (int): The line number where rawtext appears in the input.

        inliner: The inliner instance that called us.

        options(dict): Directive options for customization.

        content (list): The directive content for customization.
    """
    env = inliner.document.settings.env
    docname = env.docname
    # pprint(env.included)
    app = env.app

    parent = inliner.parent
    include = True
    if text[0] == '-':
        include = False
        text = text[1:]
    text = text.split('-')
    pn = text[0]
    dash = ''
    if len(text) == 2:
        dash = int(text[1], 10)
    matches = []
    if not dash:
        matches = [media for media in env.media_list if media['pn'] == pn]
        # If the dash wasn't specified, then there may be more than one dash number,
        # select the highest one
        if matches:
            dash=max([match['dash'] for match in matches])
    matches = [ media for media in env.media_list if media['pn'] == pn and media['dash'] == dash ]
    # If we can't find a match, print out a warning and put ??? as the dash number
    title = ''
    if not matches:
        #print('{docname} (line {lineno}):: WARNING: Media {pn} not in media list'.format(
        env.warn(docname,
                 'Media {pn} not in media list (line {lineno})'.format(pn=pn, lineno=lineno),
                 lineno=lineno,
                 )
        title = 'Unknown Disk Title'
        dash = 'UNK'
    else:
        title = matches[0]['title']
        dash='{dash:03}'.format(dash=dash)
    pn = '{pn}-{dash}'.format(pn=pn,
                                 dash=dash)
    if not env.media_titles.get(pn, ''):
        env.media_titles[pn] = title
    if include:
        env.required_media[docname] = env.required_media.get(docname, []) + [pn]
    return [nodes.Text(pn)], []


class required_media_list(nodes.General, nodes.Element):
    '''Replacement node for media list

    This class is a place holder so the replacement sections can be found during
    the build process.  This should be added the to the table.  It will add
    the :any:`nodes.tgroup` and all subcomponents.'''
    pass


class RequiredMediaListDirective(Table):
    def run(self):
        env = self.state.document.settings.env
        app = env.app
        config = app.config
        # For some reason it won't pick this up from reST, so code it in
        self.arguments = ['Required Media']
        title, messages = self.make_title()
        env.required_media_title_node = title
        table = nodes.table('', classes=['longtable'])
        table.insert(0, title)
        table += required_media_list()
        return [table]


def get_included_documents(doctree):
    '''Traverses the document tree and return the list of documents that are
    included in the provided document tree.

    Args:
        doctree (node): The document tree

    Returns:
        list: A list of :class:`str` with the document names found.
    '''
    docnames = []
    last_doc_name = ''
    for doc in doctree.traverse(nodes.document, include_self=True, descend=True, siblings=True):
        cur_docname = doc.attributes.get('docname', '')
        if cur_docname and cur_docname != last_doc_name:
            docnames.append(cur_docname)
            last_doc_name = cur_docname
        for sec in doctree.traverse(nodes.section, include_self=True, descend=True, siblings=True):
            cur_docname = sec.attributes.get('docname', '')
            if cur_docname and cur_docname != last_doc_name:
                docnames.append(cur_docname)
                last_doc_name = cur_docname
    return docnames


def get_required_media_list(env, doctree):
    '''Returns the list of required part numbers

    Args:
        env (Environment): The Sphinx env

        doctree (node): The document tree

    Returns:
        list: A list of :class:`str` with the document names found.
    '''
    included_docs = get_included_documents(doctree)
    required = []
    for docname, media in env.required_media.iteritems():
        if docname in included_docs:
            for pn in media:
                if pn not in required:
                    required.append(pn)
    return sorted(required)


def process_required_media_list_nodes(app, doctree, fromdocname):
    '''Scans the document for ``requirements_media_list`` directive and builds the
    table based on the ``media`` roles found.

    Args:
        app (application):

        doctree ():

        fromdocname (str): The name of the file where the requirements_media_list directive is
           found.

    Results:
        The directive node is replaced with a table of Required Media.
    '''
    env = app.builder.env

    for table in doctree.traverse(required_media_list):
        header = ['Part Number', 'Title']
        colwidths = (1,2)
        tgroup = nodes.tgroup(cols=len(header))
        table += tgroup
        for colwidth in colwidths:
            tgroup += nodes.colspec(colwidth=colwidth)

        thead = nodes.thead()
        tgroup += thead
        thead += create_table_row(header)

        tbody = nodes.tbody()
        tgroup += tbody

        rows_added = 0
        for pn in get_required_media_list(env, doctree):
            row = (pn,
                   env.media_titles[pn])
            tbody.append(create_table_row(row))
            rows_added += 1
        if rows_added == 0:
            row = ('N/A', 'N/A')
            tbody.append(create_table_row(row))


def equipment_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    """Creates a media role

    Returns 2 part tuple containing list of nodes to insert into the
    document and a list of system messages.  Both are allowed to be
    empty.

    Args:
        name (str): The role name used in the document.

        rawtext (str): The entire markup snippet, with role.

        text (str): The text marked with the role.

        lineno (int): The line number where rawtext appears in the input.

        inliner: The inliner instance that called us.

        options(dict): Directive options for customization.

        content (list): The directive content for customization.
    """
    env = inliner.document.settings.env
    app = env.app
    # If the media list was not previously loaded, try to load it now
    if not hasattr(env, 'equipment_list'):
        env.equipment_list = []
        fname = os.path.join(app.confdir, app.config.required_equipment_list)
        with open(fname, 'r') as f:
            env.equipment_list = yaml.load(f.read())
    if not hasattr(env, 'required_equipment'):
        env.required_equipment = {}
    pn = text
    # If we can't find a match, print out a warning
    if not match:
        #print('{docname} (line {lineno}):: WARNING: Equipment {pn} not in equipment list'.format(
        #            docname=env.docname,
        #            lineno=lineno,
        #            pn=pn))
        env.warn(docname,
                 'Equipment {pn} not in media list (line {lineno})'.format(pn=pn, lineno=lineno),
                 lineno=lineno,
                 )
    info = env.required_equipment.get(pn, {})
    if info:
        info['docnames'].append(docname)
    else:
        description = [ x['description'] for x in env.equipment_list if x['pn'] == pn ]
        env.required_equipment[pn] = {'description': description, 'docnames': [docname]}
    return [nodes.Text(pn)], []


class required_equipment_list(nodes.General, nodes.Element):
    '''Replacement node for equipment list

    This class is a place holder so the replacement sections can be found during
    the build process.  This should be added the to the table.  It will add
    the :any:`nodes.tgroup` and all subcomponents.'''
    pass


class RequiredEquipmentListDirective(Table):
    def run(self):
        env = self.state.document.settings.env
        app = env.app
        config = app.config
        # For some reason it won't pick this up from reST, so code it in
        self.arguments = ['Required Equipment']
        title, messages = self.make_title()
        env.required_media_title_node = title
        table = nodes.table('', classes=['longtable'])
        table.insert(0, title)
        table += required_equipment_list()
        return [table]


def process_required_equipment_list_nodes(app, doctree, fromdocname):
    '''Scans the document for ``required_equipment_list`` directive and builds the
    table based on the ``equipment`` roles found.

    Args:
        app (application):

        doctree ():

        fromdocname (str): The name of the file where the required_equipment_list directive is
           found.

    Results:
        The directive node is replaced with a table of Required Equipment.
    '''
    env = app.builder.env
    for table in doctree.traverse(required_equipment_list):
        header = ['Part Number', 'Description']
        colwidths = (1,2)
        tgroup = nodes.tgroup(cols=len(header))
        # table += caption
        table += tgroup
        for colwidth in colwidths:
            tgroup += nodes.colspec(colwidth=colwidth)

        thead = nodes.thead()
        tgroup += thead
        thead += create_table_row(header)

        tbody = nodes.tbody()
        tgroup += tbody


        rows_added = 0
        if not hasattr(env, 'required_equipment'):
            env.required_equipment = {}
        for pn in sorted(env.required_equipment.keys(), reverse=False):
            found = False
            info = env.required_equipment[pn]
            docnames = info['docnames']
            for docname in docnames:
                if (docname in env.included or
                    docname in env.files_to_rebuild):
                    found = True
                    break;
            if found:
                row = (pn,
                       info['description'])
                tbody.append(create_table_row(row))
                rows_added += 1
        if rows_added == 0:
            row = ('N/A', 'N/A')
            tbody.append(create_table_row(row))


def create_table_row(row_cells):
    '''Creates a basic row for a table from a list

    Args:
        row_cells (list): A list of cell contents.  If an item in the list is a
            string, then it will be wrapped in a paragraph node, otherwise it
            it added directly to a cell.

    Returns:
        nodes.row: A table row.
    '''
    row = nodes.row()
    for cell in row_cells:
        entry = nodes.entry()
        row += entry
        if isinstance(cell, str) or isinstance(cell, unicode):
            entry += nodes.paragraph(text=cell)
        else:
            entry += cell
    return row
