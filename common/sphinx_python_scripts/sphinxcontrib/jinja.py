import codecs
import os
from os import path
import re
from sets import Set
import sys
import tempfile
import time
import traceback
import urllib

from pprint import pprint

from docutils import nodes
from docutils.parsers.rst import Directive
from docutils.parsers.rst import directives
from docutils.statemachine import ViewList

from jinja2 import Template, FileSystemLoader, Environment
from jinja2 import loaders
from jinja2.utils import open_if_exists

import sphinx
from sphinx.directives.other import TocTree
from sphinx import addnodes
from sphinx.locale import versionlabels, _
from sphinx.util import url_re, docname_join
from sphinx.util.nodes import explicit_title_re, set_source_info, \
    process_index_entry
from sphinx.util.matching import patfilter

extensions = ['jinja2.ext.do',
              'jinja2.ext.loopcontrols',
              'jinja2.ext.with_',
             ]

jinja_tests =  {}
jinja_filters = {}
jinja_env = None

class JinjaDirective(Directive):

    has_content = True
    required_arguments = 1
    option_spec = {
        "file": directives.path,
        "header_char": directives.unchanged,
        "debug": directives.unchanged,
        "all-contexts": directives.unchanged,
    }
    app = None
    jinja_env = jinja_env


    def get_context_name(self, docname):
        '''Determines the context name to use based on `docname`

        If the ``jinja_rel_toctrees`` option is enabled, it will use the first
        piece of the path to determine the context name.  If that fails or
        the option is disabled, it will use the context specified by the
        directive.
        '''
        jinja_context_name = ''
        env = self.state.document.settings.env
        if env.config.jinja_rel_toctrees:
            jinja_context_name = docname.split('/')[0]
            if not env.config.jinja_contexts.get(jinja_context_name, None):
                jinja_context_name = ''
        if not jinja_context_name:
            jinja_context_name = self.arguments[0]
        return jinja_context_name


    def get_source(self, environment, template):
        '''Wrapper function for FileSystemLoader.get_source

        In order to determine the dependencies, we need to know all of the files
        that Jinja loads.  To do this, we wrap the get_source function with
        this one so we can examine the ``filename`` that was returned'''
        contents, filename, uptodate = self.loader_get_source(environment, template)
        if filename not in self.dependencies:
            self.dependencies.append(filename)
        return contents, filename, uptodate


    def run(self):
        env = self.state.document.settings.env
        jinja_context_name = self.get_context_name(env.docname)
        return self.process_node(jinja_context_name)


    def process_node(self, jinja_context_name):
        self.dependencies = []
        node = nodes.Element()
        node.document = self.state.document
        env = self.state.document.settings.env
        docname = env.docname
        env.jinja_files[docname] = {}
        print('Processing: %s' % docname)
        template_filename = self.options.get("file")
        debug_template = self.options.get("debug")
        cxt = self.app.config.jinja_contexts[jinja_context_name]
        app = self.app
        cxt["options"] = {
            "header_char": self.options.get("header_char")
        }
        template_source=u''

        try:
            # Create a Jinja Environment so we can use Jinja include and from
            # statements as well as custom tests and filters
            jinja_env = Environment(
                              loader=FileSystemLoader(
                                         [self.app.config.jinja_base,
                                          os.path.join(self.app.config.jinja_base, os.path.dirname(docname))]
                                         , followlinks=True),
                              extensions=extensions,
                          )
            # In order to figure out dependencies for caching/rendering, we need
            # be able to keep track of which files are loaded, so use monkey-
            # patching to wrap the FileSystemLoader.get_source function so
            # we can capture the filenames
            self.loader = jinja_env.loader
            self.loader_get_source = self.loader.get_source
            self.loader.get_source = self.get_source
            # Load all the custom tests
            for name, func in jinja_tests.iteritems():
                jinja_env.tests[name] = func
            # Load all the custom filters
            for name, func in jinja_filters.iteritems():
                jinja_env.filters[name] = func
            template_source = u''
            if template_filename:
                # tpl = jinja_env.get_template(template_filename)
                reference_uri = directives.uri(os.path.join(self.app.config.jinja_base, template_filename))
                template_path = urllib.url2pathname(reference_uri)
                encoded_path = template_path.encode(sys.getfilesystemencoding())
                imagerealpath = os.path.abspath(encoded_path)
                with codecs.open(imagerealpath, encoding='utf-8') as f:
                    template_source = f.read()
            else:
                template_source=u'\n'.join(self.content)
            tpl = jinja_env.from_string(u'\n'.join(self.content))
            new_content = tpl.render(**cxt)
            # Record some data about this file (context used, dependent files)
            env.jinja_files[docname]['context_name'] = jinja_context_name
            env.jinja_files[docname]['dependencies'] = []
            for dep in self.dependencies:
                env.jinja_files[docname]['dependencies'].append((dep, os.path.getmtime(dep)))
            env.jinja_files[docname]['all-contexts'] = self.options.get("all-contexts")
        except Exception as e:
            print_debug(env, 'Template Before Processing', template_source)
            env.warn('An error occured while attempting to process {docname}.  The error message is {error}'.format(docname=docname,
                                                                                                                    error=e),
                    self)
            print('\n\n')
            print('Exception Type: {}'.format(type(e)))
            print('Error message: {}'.format(e.message))
            if hasattr(e, 'lineno'):
                print('Line Number: {}'.format(e.lineno))
            print('File: {}'.format(docname))
            trace = traceback.format_exc()
            files = re.findall('File "(.*)".* in (?:top-level )?template', trace)
            if not files:
                if '<unknown>' in trace:
                    files = [ docname ]
            if files:
                print('Files include trace:')
                for f in files:
                    if f == '<template>':
                        f = docname
                    print(path.relpath(f, app.confdir))
            print('\n\n')
            new_content = ur'''
:tbd:`{error} ({errorType}) in {docname}, line {lineno} in jinja block`

.. code-block:: none
    :emphasize-lines: {lineno}
    :caption: Error "{error}" on line {lineno}

    {content}'''.format(lineno=e.lineno if hasattr(e, 'lineno') else 'unknown',
                    error=e.message,
                    errorType=type(e),
                    docname=docname,
                    content=(u'\n'+u' '*4).join(template_source.split('\n')),)
        if debug_template is not None:
            print('')
            print('********** From {} **********'.format(docname))
            print_debug(env, 'Template Before Processing', template_source)
            print_debug(env, 'Template After Processing', new_content)
            debug_code_block=u'''
:tbd:`Remove Jinja :debug: from {docname}, line {lineno}`

.. code-block:: none
    :caption: Debug Info: Input to Jinja
    :linenos:

    {input}

.. code-block:: none
    :caption: Debug Info: Output from Jinja
    :linenos:

    {output}'''.format(docname=docname,
                   lineno=self.lineno,
                   input=(u'\n'+u' '*4).join(template_source.split('\n')),
                   output=(u'\n'+u' '*4).join(new_content.split('\n')))
            new_content = u'\n'.join(
                [new_content,
                 debug_code_block])


        sphinx.util.nodes.nested_parse_with_titles(self.state,
                                                   ViewList(new_content.splitlines(), source=docname),
                                                   node)
        return node.children


def print_debug(env, header, content):
    print('')
    print('********** Begin Jinja Debug Output: {} **********'.format(header))
    print(content.encode('ascii', 'replace'))
    print('********** End Jinja Debug Output: {} **********'.format(header))
    print('')


def toctree_run(self):
    ''' Monkey patched version of sphinx.directives.other to allow for Jinja Context based directory naming'''
    env = self.state.document.settings.env
    suffixes = env.config.source_suffix
    glob = 'glob' in self.options
    caption = self.options.get('caption')
    if caption:
        self.options.setdefault('name', nodes.fully_normalize_name(caption))
    # Get a document base so it can use relative 'Jinja' env paths
    doc_base = ''
    if env.config.jinja_rel_toctrees:
        doc_base = env.docname.split('/')[0]
        if doc_base in env.config.jinja_contexts.keys():
            doc_base = ''.join(['/', doc_base])
        else:
            #print('* discarding docbase %s from %s, %s' % (doc_base, env.docname, env.config.jinja_contexts.keys()))
            doc_base = ''


    # print('** Processing %s: ' % env.docname)
    ret = []
    # (title, ref) pairs, where ref may be a document, or an external link,
    # and title may be None if the document's title is to be used
    entries = []
    includefiles = []
    all_docnames = env.found_docs.copy()
    # don't add the currently visited file in catch-all patterns
    all_docnames.remove(env.docname)
    for entry in self.content:
        if not entry:
            continue
        if glob and ('*' in entry or '?' in entry or '[' in entry):
            # First try to match to with a known Jinja Context
            orignal_entry = entry
            if entry[0] == '/' and env.config.jinja_rel_toctrees and doc_base:
                entry = ''.join([doc_base, entry])
            patname = docname_join(env.docname, entry)
            docnames = sorted(patfilter(all_docnames, patname))
            if entry[0] == '/' and env.config.jinja_rel_toctrees and not docnames:
                ret.append(self.state.document.reporter.warning(
                    'toctree glob pattern %r didn\'t match any documents, will look without %s'
                    % (entry, doc_base), line=self.lineno))
                entry = orignal_entry
                patname = docname_join(env.docname, entry)
                docnames = sorted(patfilter(all_docnames, patname))
            for docname in docnames:
                all_docnames.remove(docname)  # don't include it again
                entries.append((None, docname))
                includefiles.append(docname)
                # print('*** Adding docname %s per glob in %s' % (docname, env.docname))
            if not docnames:
                ret.append(self.state.document.reporter.warning(
                    'toctree glob pattern %r didn\'t match any documents'
                    % entry, line=self.lineno))
                # print('*** No files matching %s per glob in %s' % (entry, env.docname))
        else:
            # look for explicit titles ("Some Title <document>")
            m = explicit_title_re.match(entry)
            if m:
                ref = m.group(2)
                title = m.group(1)
                docname = ref
            else:
                ref = docname = entry
                title = None
            # remove suffixes (backwards compatibility)
            for suffix in suffixes:
                if docname.endswith(suffix):
                    docname = docname[:-len(suffix)]
                    break
            # absolutize filenames
            doc_patterns = [docname]
            if docname[0] == '/' and doc_base:
                doc_patterns = ['/'.join([doc_base, docname]), docname]
            for docname in doc_patterns:
                docname = docname_join(env.docname, docname)
                #print('*** Looking for %s' % (docname))
                if url_re.match(ref) or ref == 'self':
                    entries.append((title, ref))
                    #print('*** Adding docname %s per %s' % (docname, env.docname))
                    break
                elif docname not in env.found_docs:
                    if docname == doc_patterns[-1]:
                        ret.append(self.state.document.reporter.warning(
                            'toctree contains reference to nonexisting '
                            'document %r' % docname, line=self.lineno))
                        env.note_reread()
                else:
                    all_docnames.discard(docname)
                    entries.append((title, docname))
                    includefiles.append(docname)
                    #print('*** Adding docname %s per %s' % (docname, env.docname))
                    break
    subnode = addnodes.toctree()
    subnode['parent'] = env.docname
    # entries contains all entries (self references, external links etc.)
    subnode['entries'] = entries
    # includefiles only entries that are documents
    subnode['includefiles'] = includefiles
    subnode['maxdepth'] = self.options.get('maxdepth', -1)
    subnode['caption'] = caption
    subnode['glob'] = glob
    subnode['hidden'] = 'hidden' in self.options
    subnode['includehidden'] = 'includehidden' in self.options
    subnode['numbered'] = self.options.get('numbered', 0)
    subnode['titlesonly'] = 'titlesonly' in self.options
    set_source_info(self, subnode)
    wrappernode = nodes.compound(classes=['toctree-wrapper'])
    wrappernode.append(subnode)
    self.add_name(wrappernode)
    ret.append(wrappernode)
    return ret


def split_template_path(template):
    """
    This is a Monkey Patched version of jinja2.loaders.split_template_path that
    allows for loading parent directories (..).  Since we are using this for
    generating documentation, it should not pose any security issues like it
    would on a web server.
    """
    pieces = []
    for piece in template.split('/'):
        if path.sep in piece \
           or (path.altsep and path.altsep in piece):
            raise TemplateNotFound(template)
        elif piece and piece != '.':
            pieces.append(piece)
    return pieces


def test_match(value, other):
    '''A function that is the same as :func:`re.match`

    Args:
        value (str): The value you are testing
        other (str): The regular expression to test against

    Returns:
        bool: True if :func:`re.match` returns a :class:`re.MatchObject`, False
              otherwise
    '''
    if not value:
        value = ''
    if not re.match(other, value):
        return False
    return True


def test_search(value, other):
    '''A function that is the same as :func:`re.search`

    Args:
        value (str): The value you are testing
        other (str): The regular expression to test against

    Returns:
        bool: True if :func:`re.search` returns a :class:`re.MatchObject`, False
              otherwise
    '''
    if not value:
        value = ''
    if not re.search(other, value):
        return False
    return True


def test_startswith(value, other):
    '''A function that is the same as :func:`str.startswitch`

    Args:
        value (str): The value you are testing
        other (str): The prefix(es) to look for.  Other can be a tuple of :class:`str`

    Returns:
        bool: Return True if value starts with the other, otherwise return False. other can also be a tuple of prefixes to look for.
    '''
    if not value:
        value = ''
    return value.startswith(other)


def test_endswith(value, other):
    '''A function that is the same as :func:`str.endswitch`

    Args:
        value (str): The value you are testing
        other (str): The suffix(es) to look for.  Other can be a tuple of :class:`str`

    Returns:
        bool: Return True if value ends with the other, otherwise return False. other can also be a tuple of suffixes to look for.
    '''
    if not value:
        value = ''
    return value.endswith(other)


def test_in(value, other):
    '''A function that is the same as the built in "in" command.  Works on iterable

    Args:
        value (iterable): The value you are testing
        other (iterable): The iterable in which to search for ``value``

    Returns:
        bool: Return :bool:`True` if "value in other", else :bool:`False`
    '''
    if not value:
        value = ''
    other = list(other)
    return value in other


def test_not_in(value, other):
    '''A function that is the same as the built in "in" command.  Works on iterable

    Args:
        value (iterable): The value you are testing
        other (iterable): The iterable in which to search for ``value``

    Returns:
        bool: Return :bool:`True` if "value not in other", else :bool:`False`
    '''
    if not value:
        value = ''
    other = list(other)
    return value in other


def filter_intersection(l1, l2):
    retval = []
    l2 = list(l2)
    print("is %s in %s" %(l1, l2))
    for i in l1:
        if i in l2:
            retval.append(i)
    return retval


def filter_difference(l1, l2):
    retval = []
    l2 = list(l2)
    print("is %s not in %s" %(l1, l2))
    for i in l1:
        if i not in l2:
            retval.append(i)
    return retval


def env_get_outdated(app, env, added, changed, removed):
    '''Determines the list of files that use Jinja that need to be rerendered

    To determine if a file has "changed", it needs to evaluate the following
    items:

    * Has the context been changed since this file was last built?
    * Has any of the dependent files (sourced via jinja import/include been
      modified since the last successful build?
    '''
    changed = []
    for docname, doc_info in env.jinja_files.copy().iteritems():
        if docname not in env.found_docs:
            env_purge_doc(app, env, docname)
            print('Removing %s from history' % docname)
            continue
        last_built = doc_info.get('last_built', 0)
        jinja_context_name = doc_info.get('context_name', '')
        context_last_modified = 0
        if doc_info.get('all-contexts', ''):
            for cxt in app.config.jinja_contexts:
                if not app.config.jinja_contexts.get(jinja_context_name, {}).get('last_modified', 0) or \
                   env.jinja_contexts_timestamp[cxt] != app.config.jinja_contexts.get(jinja_context_name, {}).get('last_modified', 0):
                    changed.append(docname)
                    continue
        else:
            if not app.config.jinja_contexts.get(jinja_context_name, {}).get('last_modified', 0) or \
               env.jinja_contexts_timestamp[jinja_context_name] != app.config.jinja_contexts.get(jinja_context_name, {}).get('last_modified', 0):
                changed.append(docname)
                continue

        for fname, mtime in doc_info.get('dependencies', []):
            try:
                if os.path.getmtime(fname) != mtime:
                    changed.append(docname)
                    continue
            except:
                changed.append(docname)
                continue
    env.jinja_last_build_time = env.jinja_build_start_time
    for cxt in app.config.jinja_contexts:
        env.jinja_contexts_timestamp[cxt] = app.config.jinja_contexts[cxt].get('last_modified',0)
    return changed


def env_purge_doc(app, env, docname):
    '''Removes the cached information about `docname`'''
    env.jinja_files.pop(docname, None)


def builder_inited(app):
    '''Initializes the environment for the Jinja extension'''
    env = app.builder.env
    # Create a dictionary to keep track of the files with Jinja directives
    # and a little bit of information about each source file
    if not hasattr(env, 'jinja_files'):
        env.jinja_files = {}
    # Determine the last successful build time so we know which content we
    # need to reprocess
    if not hasattr(env, 'jinja_last_build_time'):
        env.jinja_last_build_time = 0
    # Mark the start time, so we can change it to the last_build_time if this
    # build is successful
    env.jinja_build_start_time = time.time()
    if not hasattr(env, 'jinja_contexts_timestamp'):
        env.jinja_contexts_timestamp = {}
    for cxt in app.config.jinja_contexts:
        if not env.jinja_contexts_timestamp.get(cxt, 0):
            env.jinja_contexts_timestamp[cxt] = 0


def build_finished(app, exception):
    '''Updates the last successful build time upon document completion'''
    env = app.builder.env
    if not exception:
        print('Updating jinja modification time cache...')
        env.jinja_last_build_time = env.jinja_build_start_time
        for cxt in app.config.jinja_contexts:
            env.jinja_contexts_timestamp[cxt] = app.config.jinja_contexts[cxt].get('last_modified',0)

        if app.config.jinja_dump_file_list:
            print('Updating included document list')
            relations = env.collect_relations()

            included_docs = []
            # Each relation is a list in the following order [parent, prev_doc, next_doc]
            cur_doc = env.config.master_doc
            docs = env.jinja_files.copy()
            while cur_doc:
                included_docs.append(os.path.abspath(cur_doc) + '.rst')
                for fname in docs.get(cur_doc, {}).get('dependencies', []):
                    included_docs.append(fname[0])
                doctree = env.get_doctree(cur_doc)
                cur_doc = relations[cur_doc][2]
            temp_fname = os.path.join(app.builder.outdir, '../included_files.txt')
            try:
                f = open(temp_fname, 'w')
                f.writelines('\n'.join(included_docs))
                f.close()
            except:
                print('Error, unable to write to ' + temp_fname)
                traceback.print_exc()


def setup(app):
    JinjaDirective.app = app
    app.add_directive('jinja', JinjaDirective)
    app.add_config_value('jinja_contexts', {}, '')
    app.add_config_value('jinja_base', os.path.abspath('.'), '')
    app.add_config_value('jinja_rel_toctrees', True, '')
    app.add_config_value('jinja_dump_file_list', False, '')
    loaders.split_template_path = split_template_path
    TocTree.run = toctree_run
    jinja_tests.update(
        {'match': test_match,
         'search': test_search,
         'startswith': test_startswith,
         'endswith': test_endswith,
         'in': test_in,
         'not_in': test_not_in,
        })
    jinja_filters.update(
        {'intersection': filter_intersection,
         'difference': filter_difference,
        })
    app.connect('env-get-outdated', env_get_outdated)
    app.connect('env-purge-doc', env_purge_doc)
    app.connect('builder-inited', builder_inited)
    app.connect('build-finished', build_finished)
