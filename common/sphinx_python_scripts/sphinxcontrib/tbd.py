# .. note:: This doesn't appear to be able to resolve the tree out of a list
#           table.  So if you put a TBD in a list able, you won't be able it see
#           the section in the TBD table :(

from docutils import nodes
from docutils.parsers.rst.directives.tables import Table
from docutils.parsers.rst import directives
from docutils.parsers.rst import Directive

from pprint import pprint

def setup(app):
    app.add_config_value('include_tbds', True, 'env')

    app.add_node(tbdlist,
                 html=(visit_pass, depart_pass),
                 latex=(visit_pass, depart_pass),
                 text=(visit_pass, depart_pass))
    app.add_node(tbd,
                 html=(visit_tbd_node, depart_tbd_node),
                 latex=(visit_tbd_node, depart_tbd_node),
                 text=(visit_tbd_node, depart_tbd_node))
    app.add_directive('tbd', TbdDirective)
    app.add_directive('tbdlist', TbdlistDirective)
    app.add_role('tbd', tbd_role)
    # app.add_role('tbd', TbdRole)
    app.connect('doctree-resolved', process_tbd_nodes)
    app.connect('env-purge-doc', purge_tbds)

    return {'version': '0.1'}
    
    
def visit_pass(self, node):
    pass

    
def depart_pass(self, node):
    pass
    
    
def tbd_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    """Creates a TBD role

    Returns 2 part tuple containing list of nodes to insert into the
    document and a list of system messages.  Both are allowed to be
    empty.

    Args:
        name (str): The role name used in the document.
        
        rawtext (str): The entire markup snippet, with role.
    
        text (str): The text marked with the role.
    
        lineno (int): The line number where rawtext appears in the input.
    
        inliner: The inliner instance that called us.
    
        options(dict): Directive options for customization.
    
        content (list): The directive content for customization.
    """
    env = inliner.document.settings.env
    app = env.app
    if not hasattr(env, 'all_tbds'):
        env.all_tbds = []    
    parent = inliner.parent
    # Look through the parents until we can find a section id for this one
    sectionid = ''
    section_title=''
    for tuple_ in inliner.document.ids.iteritems():
        for node in tuple_:
            if (type(node) == nodes.section or 
                type(node) == nodes.document):
                sectionid = node.attributes['ids']
                section_title = get_section_title(node)
                break
    #while parent:
    #    if hasattr(parent, 'attributes'):
    #        if parent.attributes.get('ids', []):
    #            sectionid = parent.attributes['ids']
    #            break
        #pprint(parent.document.attributes)
        #pprint(dir(parent.document))
    #    parent = parent.parent
    docname = env.docname
    targetid = "tbd-%d" % env.new_serialno('tbd')
    targetnode = nodes.target(ids=[targetid], 
                              names=[docname], 
                              refuri="/" + docname,
                              refid=targetid,)
    #inliner.document.note_explicit_target(targetnode)
    #targetnode = nodes.target('', '', ids=[targetid])
    inliner.document.note_explicit_target(targetnode)
    
    env.all_tbds.append({
        'docname': env.docname,
        'target': targetnode,
        'content': text,
        'lineno': lineno,
        'sectionid': sectionid,
        'sectiontitle': section_title,
        'parent': inliner.parent,
        })
     
    
    node = nodes.Text('TBD')
    return [targetnode, node], []        
       
    
class tbd(nodes.target, nodes.Element):
    pass

    
class tbdlist(nodes.General, nodes.Element):
    pass

    
def visit_tbd_node(self, node):
    self.visit_node(node)

    
def depart_tbd_node(self, node):
    self.visit_node(node)    
    

class TbdlistDirective(Table):
    def run(self):
        env = self.state.document.settings.env
        app = env.app
        config = app.config
        # For some reason it won't pick this up from reST, so code it in
        self.arguments = ['TBDs']
        title, messages = self.make_title()
        env.required_media_title_node = title
        table = nodes.table('', classes=['longtable'])
        table.insert(0, title)
        table += tbdlist()
        return [table]    
        

class TbdDirective(Directive):
    '''The class for a TbdDirective'''
    
    has_content = True

    def run(self):
        '''Add the TBD directive to the running list of TBDs
        
        Args:
            self (TbdDirective): This node
        
        Results:
            The node and its information is added to the all_tbds list for this
            app.
        '''
        env = self.state.document.settings.env

        targetid = "tbd%d" % env.new_serialno('tbd')
        targetnode = nodes.target('', '', ids=[targetid])

        # If this is the first node found, create the list
        if not hasattr(env, 'all_tbds'):
            env.all_tbds = []
        # Add this node to the list
        env.all_tbds.append({
            'docname': env.docname,
            'lineno': self.lineno,
            'target': targetnode,
            'content': self.content,
            'node': self.state_machine.node,
            'sectionname': self.state_machine.node.attributes['names'],
            'sectionid': self.state_machine.node.attributes['ids'],
            'sectiontitle': get_section_title(self.state_machine.node),
        })
        return [targetnode]

        
def get_section_title(node):
    '''Returns the section title
    
    This function traverses up the document tree until a ``section`` or 
    ``document`` node is found.  Once found, it searches that node for a title.
    The title is returned if found, otherwise an empty string is returned.
    
    Args:
        node (node): The node to start looking for a section title.
        
    Returns:
        str: The section title if found, or an empty string if not found.
    '''
    
    # Traverse up the document tree until we find a ``section`` or ``document``
    if (type(node) != nodes.section and 
        type(node) != nodes.document):
        while node:
            if (type(node) == nodes.section or 
                type(node) == nodes.document):
                break
            node = node.parent
        # Now try to find the first title node
    if node:
        title_nodes = node.traverse(condition=nodes.title, include_self=True)
        if title_nodes:
            # Find the text node with the title in it:
            text_nodes = title_nodes[0].traverse(condition=nodes.Text)
            if text_nodes:
                return text_nodes[0].astext()
    return ''

        
def purge_tbds(app, env, docname):
    if not hasattr(env, 'all_tbds'):
        return
    env.all_tbds = [tbd for tbd in env.all_tbds
                          if tbd['docname'] != docname]        
                          

def process_tbd_nodes(app, doctree, fromdocname):
    '''Scans the document for tbd directives and roles to build a TBD Table
    
    Args:
        app (application):
        
        doctree ():
        
        fromdocname (str): The name of the file where the tbdlist directive is
           found.
    
    Results:
        The directive node is replaced with a table of TBDs.
    '''
    if not app.config.include_tbds:
        for node in doctree.traverse(tbd):
            node.parent.remove(node)
    env = app.builder.env
    for table in doctree.traverse(tbdlist):
        if not app.config.include_tbds:
            node.replace_self([])
            continue
        content = []

        header = ['Section Title', 'Context', 'Resolution Path']
        colwidths = (1,2,3)
        tgroup = nodes.tgroup(cols=len(header))
        table += tgroup
        for colwidth in colwidths:
            tgroup += nodes.colspec(colwidth=colwidth)
        
        thead = nodes.thead()
        tgroup += thead
        thead += create_table_row(header)
        
        tbody = nodes.tbody()
        tgroup += tbody
        content.append(table)
        
        #for tbd_info in sorted(env.all_tbds, reverse=False):
        count = 0
        if not hasattr(env, 'all_tbds'):
            env.all_tbds = []
        for tbd_info in sorted(env.all_tbds, reverse=False):
            if (tbd_info['docname'] not in env.included and
                tbd_info['docname'] not in env.files_to_rebuild):
                continue
            #print('INFO: TBD from {} is being included'.format(tbd_info['docname']))    
            para = nodes.paragraph()
            filename = env.doc2path(tbd_info['docname'], base=None)
            cell_contents = tbd_info['content']
            desc_para = nodes.paragraph(text=tbd_info['target'].parent.astext())

            newnode = nodes.reference('', '')
            innernode = nodes.emphasis(tbd_info['sectiontitle'], tbd_info['sectiontitle'])
            newnode['refdocname'] = tbd_info['docname']
            try:
                newnode['refuri'] = app.builder.get_relative_uri(
                    fromdocname, tbd_info['docname'])
            except Exception as e:
                env.warn(tbd_info['docname'],
                    'Unable find tbd link from {docname} (line: {lineno})'.format(docname=tbd_info['docname'], lineno=tbd_info['lineno']),
                    lineno=tbd_info['lineno'],
                    )
                print('unable to process tbd in {}'.format(tbd_info['docname']))
                continue
            #pprint(tbd_info['target'])
            #newnode['refuri'] += '#' + tbd_info['target']['refid']
            newnode.append(innernode)
            para += newnode
            row = (para,
                   desc_para,
                   nodes.paragraph(text=tbd_info['content']))
            tbody.append(create_table_row(row))
            count += 1
        if count == 0:
            row = ('N/A',
                   'N/A',
                   'N/A')
            tbody.append(create_table_row(row))        

def create_table_row(row_cells):
    '''Creates a basic row for a table from a list
    
    Args:
        row_cells (list): A list of cell contents.  If an item in the list is a
            string, then it will be wrapped in a paragraph node, otherwise it
            it added directly to a cell.
        
    Returns:
        nodes.row: A table row.
    '''
    row = nodes.row()
    for cell in row_cells:
        entry = nodes.entry()
        row += entry
        if isinstance(cell, str):
            entry += nodes.paragraph(text=cell)
        else:
            entry += cell
    return row