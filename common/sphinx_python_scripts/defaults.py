import re
import sphinx
import tablib
import ciscoconfparse
import yml2json

# -- General configuration ------------------------------------------------
# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.imgmath',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
#    'sphinx-docxbuilder',
    'jinja',
    'tbd',
    'requirements',
    'sphinxcontrib.seqdiag',
    'sphinxcontrib.nwdiag',
    'sphinxcontrib.rackdiag',
    'sphinxcontrib.packetdiag',
    'sphinxcontrib.blockdiag',
    'sphinxcontrib.actdiag',
    'sphinx_git',
    'sphinxcontrib.plantuml',
    'sphinxcontrib.jupyter',
    'sphinxcontrib.ansibleautodoc',
    'sphinxcontrib.jsonschema',
    'sphinxcontrib.confluencebuilder',
    'sphinx.ext.extlinks',
    'sphinx.ext.githubpages',
#    'sphinx.ext.linkcode',
]

# Configuration settings for imgmath
imgmath_image_format = "svg"
# Configuration settings for seqdiag
seqdiag_fontpath = '/usr/share/fonts/microsoft/calibri.ttf'
seqdiag_html_image_format = "SVG"
seqdiag_latex_image_format = "PDF"

# Configuration settings for nwdiag, rackdiag(nwdiag), packetdiag(nwdiag)
nwdiag_fontpath = '/usr/share/fonts/microsoft/calibri.ttf'
nwdiag_html_image_format = "SVG"
nwdiag_latex_image_format = "PDF"
rackdiag_fontpath = '/usr/share/fonts/microsoft/calibri.ttf'
rackdiag_html_image_format = "SVG"
rackdiag_latex_image_format = "PDF"
packetdiag_fontpath = '/usr/share/fonts/microsoft/calibri.ttf'
packetdiag_html_image_format = "SVG"
packetdiag_latex_image_format = "PDF"

# Configuration settings for blockdiag
blockdiag_fontpath = '/usr/share/fonts/microsoft/calibri.ttf'
blockdiag_html_image_format = "SVG"
blockdiag_latex_image_format = "PDF"

# Configuration settings for actdiag
actdiag_fontpath = '/usr/share/fonts/microsoft/calibri.ttf'
actdiag_html_image_format = "SVG"
actdiag_latex_image_format = "PDF"

# Configuration settings for plantuml
plantuml_output_format = "svg"
plantuml_latex_output_format = "pdf"
plantuml = "java -jar 'C:\cygwin64\java\plantuml.jar'"

def guess_system_type(context_name):
    # type: (str) -> str
    '''guess_system_type(context_name)

    Attempts to guess the system type for the YAML context name.  Possible
    return values:

    * [s|ts]enclave
    * mini_[s|ts]enclave
    * mini_infra
    * mce
    * lrs
    '''
    import re
    security_level = ''
    system_type = ''
    # See if there is a security label in the context name (['s', 'ts'])
    if context_name.startswith('s'):
        security_level = 's'
        context_name = context_name[1:]
    elif context_name.startswith('ts'):
        security_level = 'ts'
        context_name = context_name[2:]
    if context_name.startswith('b'):
        context_name = context_name[1:]
    if context_name.startswith(('mm', 'rar')):
        if security_level:
            system_type = 'mini_' + security_level + 'enclave'
        elif re.search('[0-9]+$', context_name):
            system_type = 'mini_string'
        else:
            system_type = 'mini_infra'
    elif context_name.startswith('mce'):
        if security_level:
            system_type = security_level + 'enclave'
        else:
            system_type = 'mce'
    elif context_name.startswith('lrs'):
        system_type = 'lrs'
    elif 'enclave' in context_name:
        system_type = security_level + 'enclave'
    elif context_name == 'mce':
        system_type = 'mini_infra'
    if not system_type:
        system_type = 'NONE'
    return system_type

################################################################################
# Setup CM Status
################################################################################
cmstatus='Non-CM'
if is_in_svn('.'):
    cmstatus = get_svn_status()
if is_in_git('.'):
    cmstatus = get_git_status()
    gitstatus = 'inGit'

################################################################################
# Configure the Jinja Contexts
################################################################################
# Enable using of include statements in jinja
jinja_base = os.path.abspath('.')
contexts_list = []

if not 'jinja_contexts' in globals() or not jinja_contexts:
    jinja_contexts = {
        'signatures': {'signatures': yaml.load(open('Signatures.yaml').read())},
        'revision_history': {'revision_history': yaml.load(open('Revision_History.yaml').read())},
    }

    for context in contexts:
        ycp = None
        try:
            print('Loading configuration files using: {}'.format(context))
            doc_type = ''
            system_type = ''
            if isinstance(context, tuple):
                if len(context) == 3:
                    ycp = yaml_config_parser(context[2])
                elif len(context) == 4:
                    ycp = yaml_config_parser(context[2], variables_file=context[3])
                else:
                    raise ValueError('The number of values in {} is incorrect, there should be 2 or 3 values'.format(context[0]))
                system_description = context[0]
                context_name = context[1]

            elif isinstance(context, dict):
                system_description = context.get('description', '')
                context_name = context.get('context_name', '')
                doc_type = context.get('doc_type', '')
                config_path = context.get('config_path', '')
                variables_file = context.get('variables_file', '')
                system_type = context.get('system_type', '')
                ycp = yaml_config_parser(config_path, variables_file=variables_file)
            else:
                raise ValueError('Unknown context type for {}'.format(context))

            if not doc_type:
                if re.search('[Bb]uild', project):
                    doc_type = 'build'
                elif re.search('[Uu]pgrade', project):
                    doc_type = 'upgrade'
                else:
                    doc_type = 'unknown'
            if not system_type:
                system_type = guess_system_type(context_name)
            ycp.load_configuration()
            jinja_contexts[context_name] = ycp.config
            if not jinja_contexts[context_name]:
                jinja_contexts[context_name] = {}
            contexts_list.append({'name': context_name,
                                  'context': jinja_contexts[context_name]})

            jinja_contexts[context_name].update({'release': release,
                                               'version': version,
                                               'system_description': system_description,
                                               'context_name': context_name,
                                               'system_type': system_type,
                                               'contexts_list': contexts_list,
                                               'doc_type': doc_type,
                                               'gitstatus': gitstatus,
                                             })
            if 'configuration_files_media_pn' in globals():
                jinja_contexts[context_name].update({'configuration_files_media_pn': configuration_files_media_pn})
            if 'project' in globals():
                jinja_contexts[context_name].update({'project': project})
            # I'd like to get rid of this, but it would break older documents -- PJB 20171026
            if context == contexts[0]:
                jinja_contexts['yaml_config'] = jinja_contexts[context_name]
            # To determine if the configuration files have been changed, take the
            # average timestamp.  So if any file changes it will trigger a
            # rebuild.  Think should catch it 99+% of the time.
            last_modified = 0
            for fname in ycp.file_order:
                last_modified = last_modified + os.path.getmtime(fname)
            if ycp.file_order:
                last_modified = last_modified / len(ycp.file_order)
                jinja_contexts[context_name].update({'last_modified': last_modified,})
        except Exception as e:
            print(e)
            raise(Exception('An error occurred while trying to load the Jinja Context for {}'.format(context)))
# For our oldest documents (like 1.3.1 or early 1.3.2/VM1.1) we didn't have
# multiple contexts, this needs to stay here unless those documents are refactored
# to use a newer format.  It shouldn't be that hard, but who has time -- PJB 20171026
else:
    for context in jinja_contexts:
        contexts_list.append((context, {}))
        jinja_contexts[context].update({'release': release,
                                   'version': version,
                                   'system_description': '',
                                   'context_name': '',
                                   'system_type': '',
                                   'contexts_list': contexts_list,
                                    'gitstatus': gitstatus,
                                 })

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

rst_prolog = """
.. |project| replace:: {project}

.. only:: html

    Built from: {cmstatus}

""".format(project=project,
           cmstatus=cmstatus)

# The encoding of source files.
#source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

file_name = '_'.join([documentnumber,
                      'Rev',
                      document_rev,
                      project.replace(' ', '_'),
                      release,])

numfig = True
numfig_format = {'figure': 'Figure %s',
                 'table': 'Table %s',
                 'code-block': 'Code %s',
                }


# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This patterns also effect to html_static_path and html_extra_path
exclude_patterns = []

# The reST default role (used for this markup: `text`) to use for all
# documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
#keep_warnings = False

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True

highlight_languange = 'shell-session'

# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#html_theme = 'alabaster'
#html_theme = 'sphinxdoc'
html_theme = 'sphinx_rtd_theme'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#html_theme_options = {}

# Add any paths that contain custom themes here, relative to this directory.
#html_theme_path = []

# The name for this set of Sphinx documents.
# "<project> v<release> documentation" by default.
#html_title = project

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#html_logo = None

# The name of an image file (relative to this directory) to use as a favicon of
# the docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Add any extra paths that contain custom files (such as robots.txt or
# .htaccess) here, relative to this directory. These files are copied
# directly to the root of the documentation.
#html_extra_path = []

# If not None, a 'Last updated on:' timestamp is inserted at every page
# bottom, using the given strftime format.
# The empty string is equivalent to '%b %d, %Y'.
#html_last_updated_fmt = None

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
html_use_smartypants = False

# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Language to be used for generating the HTML full-text search index.
# Sphinx supports the following languages:
#   'da', 'de', 'en', 'es', 'fi', 'fr', 'hu', 'it', 'ja'
#   'nl', 'no', 'pt', 'ro', 'ru', 'sv', 'tr', 'zh'
#html_search_language = 'en'

# A dictionary with options for the search language support, empty by default.
# 'ja' uses this config value.
# 'zh' user can custom change `jieba` dictionary path.
#html_search_options = {'type': 'default'}

# The name of a javascript file (relative to the configuration directory) that
# implements a search results scorer. If empty, the default will be used.
#html_search_scorer = 'scorer.js'

# Output file base name for HTML help builder.
htmlhelp_basename = file_name

# -- Options for LaTeX output ---------------------------------------------

# Change the least significant rev to be x, eg 1.4.4 -> 1.4.x or 1.7.2 -> 1.7.x
#version = sphinx.__version__.split('.')
#version[-1] = 'x'
#version = '.'.join(version)

#PREAMBLE = string.Template(open(
#                                os.path.join(os.path.abspath('.'), os.path.sep.join(['latex_templates', version, 'preamble.tex']))
#                               ).read())

PREAMBLE = string.Template(open(r'./latex_templates/preamble.tex').read())

latex_docclass = {
    'howto': 'article',
    'manual': 'report',
}

latex_contents = r'''
    \setupHeadFootForFrontMatter
    \formattoc
    \maketitle
    \signaturepage
    \revisionhistory
    \tableofcontents
    \clearpage
    \listoffigures
    \clearpage
    \listoftables
    \clearpage
    \setupHeadFootForText
    \pagenumbering{arabic}
    \pagestyle{plain}

'''

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
'papersize': 'letterpaper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',



    'sphinxsetup': 'hmargin={.5in,.5in}, vmargin={1.3in,1.1in}',

# Additional stuff for the LaTeX preamble.
    'preamble': PREAMBLE.substitute(address='17095 Via Del Campo Court\\\\\nSan Diego, CA 92127-2412',
                                    classification=classification,
                                    docrevision=document_rev,
                                    documentnumber=documentnumber,
                                    signatures=array_to_latex_table(yaml.load(open('Signatures.yaml').read()), grid=True),
                                    revisionhistory=array_to_latex_table(yaml.load(open('Revision_History.yaml').read()), grid=False),
                                    cmversion=cmstatus
                                   ),

    'tableofcontents': latex_contents,

    # Latex figure (float) alignment
    #'figure_align': 'htbp',

    # Make this not look like a book
    'classoptions': ',openany,oneside',

    'babel': r'\usepackage[english]{babel}',

    #'latex_top_level_sectioning': 'part',
    #'fncychap': '',
    'passoptionstopackages': r'\PassOptionsToPackage{svgnames}{xcolor}',
}
# print(latex_elements)
latex_top_level_sectioning= 'part'

#latex_additional_files = ['./latex_templates/ngc_procedure.sty',
#                          './latex_templates/titlelogo.png',
#                          #'common/templates/iftex.sty',
#                         ]

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, '{}.tex'.format(file_name), project,
     u'Northrop Grumman Corp', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
latex_logo = './latex_templates/ngc_logo_blue.pdf'

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
latex_use_parts = False

# If true, show page references after internal links.
latex_show_pagerefs = True

# If true, show URL addresses after external links.
#latex_show_urls = False

# Documents to append as an appendix to all manuals.
latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, file_name, project,
     [author], 1)
]

# If true, show URL addresses after external links.
#man_show_urls = False


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, file_name, project,
     author, file_name, 'One line description of project.',
     'Miscellaneous'),
]

# Documents to append as an appendix to all manuals.
#texinfo_appendices = []

# If false, no module index is generated.
#texinfo_domain_indices = True

# How to display URL addresses: 'footnote', 'no', or 'inline'.
#texinfo_show_urls = 'footnote'

# If true, do not generate a @detailmenu in the "Top" node's menu.
#texinfo_no_detailmenu = False


# -- Options for Epub output ----------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project
epub_author = author
epub_publisher = author
epub_copyright = copyright

# The basename for the epub file. It defaults to the project name.
#epub_basename = project

# The HTML theme for the epub output. Since the default themes are not
# optimized for small screen space, using the same theme for HTML and epub
# output is usually not wise. This defaults to 'epub', a theme designed to save
# visual space.
#epub_theme = 'epub'

# The language of the text. It defaults to the language option
# or 'en' if the language is not set.
#epub_language = ''

# The scheme of the identifier. Typical schemes are ISBN or URL.
#epub_scheme = ''

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#epub_identifier = ''

# A unique identification for the text.
#epub_uid = ''

# A tuple containing the cover image and cover page html template filenames.
#epub_cover = ()

# A sequence of (type, uri, title) tuples for the guide element of content.opf.
#epub_guide = ()

# HTML files that should be inserted before the pages created by sphinx.
# The format is a list of tuples containing the path and title.
#epub_pre_files = []

# HTML files that should be inserted after the pages created by sphinx.
# The format is a list of tuples containing the path and title.
#epub_post_files = []

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']

# The depth of the table of contents in toc.ncx.
#epub_tocdepth = 3

# Allow duplicate toc entries.
#epub_tocdup = True

# Choose between 'default' and 'includehidden'.
#epub_tocscope = 'default'

# Fix unsupported image types using the Pillow.
#epub_fix_images = False

# Scale large images.
#epub_max_image_width = 0

# How to display URL addresses: 'footnote', 'no', or 'inline'.
#epub_show_urls = 'inline'

# If false, no index is generated.
#epub_use_index = True


# Example configuration for intersphinx: refer to the Python standard library.
# intersphinx_mapping = {'https://docs.python.org/': None}
