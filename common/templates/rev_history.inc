.. jinja:: revision_history

    {% if not revision_history %}{% set revision_history = {} %}{% endif %}
    --------------------
    Revision History
    --------------------

    .. list-table::
        :header-rows: 1

        * - Rev
          - Technical Reason
          - Sections
            Affected
          - Release
            Date
        {%- for row in revision_history -%}
            {%- for column in row -%}
                {%- if loop.first %}
        * - {{ column if column != none else '' }}
                {%- else %}
          - {{ column if column != none else '' }}
                {% endif -%}
            {%- endfor -%}
        {%- endfor %}

    :ref:`Git Changelog for Document <git_changelog_doc>`
