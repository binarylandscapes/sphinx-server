.. jinja:: signatures

    {% if not signatures %}{% set signatures = {} %}{% endif %}
    --------------------
    Signature Table
    --------------------

    .. list-table::
        :header-rows: 1

        * - Name
          - Title
          - Signature/Date
        {%- for row in signatures -%}
            {%- for column in row -%}
                {%- if loop.first %}
        * - {{ column if column != none else '' }}
                {%- else %}
          - {{ column if column != none else '' }}
                {% endif -%}
            {%- endfor -%}
        {%- endfor %}
