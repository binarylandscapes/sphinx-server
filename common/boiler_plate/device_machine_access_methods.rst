:orphan:

===================================
Device/Machine Access Methods
===================================

During the execution of this procedure, the operator will have to access a variety of systems, and may have to switch between them.  In order to minimize this, it is recommended that a combination of remote access methods be used as appropriate.  Here is a list of the supported remote access methods:

* Direct Machine Access using Mouse and Keyboard or KVM Console
* Direct Serial Console based
* Secure Shell (SSH) Tunnel Serial Console based
* SSH Based
* HTTP/HTTPS Web/Software Interface based
* Built-in Touch Screen

While it is recommended to utilize direct machine access to a physical machine or device to perform these functions, it is permissible to utilize any of the methods listed above unless restricted by a particular section. Some functions require having physical access to device if an issue occurs to manually power cycle and so forth.

*****************************************************************
Direct Machine Access using Mouse and Keyboard or KVM Console
*****************************************************************

Method is using physical access to machine or device by using a directly connected keyboard, mouse, and display or a Keyboard/Video/Mouse (KVM) console.  If the system contains R7910 workstations that are configured by PCoIP, than the local console may be disabled in the BIOS.  If required, it can be re-enabled.

*****************************************************************
PCoIP
*****************************************************************

Workstations can be configured for PCoIP, which uses a zero client to remote the KVM to remote location. If they have a PCoIP Remote Host Card Installed.

*********************************
Direct Serial Console based
*********************************

Method is using a DB9 to RJ45 cable or a RJ45 to RJ45 Serial Rollover cable to directly connect Domain Controller 1 or assigned computer to device directly and using PuTTy to provide a serial terminal.

************************************
SSH Tunnel Serial Console based
************************************

Method is using Cygwin or PuTTy SSH terminal to connect over IP network to assigned port of IOLAN or Avocent console remotely that provides a serial console connection. This requires the IOLAN or Avocent console configuration to be complete.

*******************
SSH based
*******************
Method is using Cygwin or PuTTy SSH terminal to connect over IP network to configured device that supports SSH protocol.

********************************************
HTTP/HTTPS Web/Software Interface based
********************************************

Method is using installed web browser Mozilla Firefox or installed software to connect over IP network to configured device that supports HTTP or HTTPS protocol.

************************
Built-in Touch Screen
************************

Method is using a built-in touch screen menu driven system to perform functions.
