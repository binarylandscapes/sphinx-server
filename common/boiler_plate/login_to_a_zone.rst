:orphan:

=============================
Login to a Zone (Solaris 11)
=============================
In some of the sections, it requires you to login to one of the Solaris zones.  In order login to the zone, as root, run the following command replacing zoneName with the actual zone name:

.. parsed-literal::

    # zlogin *zoneName*

For example to login to ``msnzone01``, the command would be:

.. code-block:: none

    # zlogin msnzone01

There is a special case of a zone cluster.  Each node has its own hostname, but the zone name across the cluster is the same.  For example, the zone cluster `dbzca`` has 3 nodes: ``dbzca01`` runs on ``appserver01`` and  ``dbzca02`` runs on ``appserver02``, and so on.  To login to ``dbzca01``, you need to be on ``appserver01`` and run the following command::

    zlogin dbzca

To login to ``dbzca02``, you would run the exact same command, but from ``appserver02``.
