:orphan:

==================
Safety Precautions
==================

Warnings, Cautions, and Notes will be used throughout this procedure to emphasize actions or conditions which may endanger personnel or equipment, or to require special attention to procedural detail, as described in the following:

.. warning::

	This is a condition or sequence which, if not strictly observed, could result in personnel injury or death or long term health hazards.

.. caution::

	This is a condition or sequence which, if not strictly observed, could result in loss of or damage to equipment or property.

.. note::
	
	This is a condition or sequence which requires special explanations or special attention to detail to ensure successful task accomplishment and achieving expected results. This also may be called out with a single * denoted an optional step or a step dependent on hardware configuration requiring research by user.
