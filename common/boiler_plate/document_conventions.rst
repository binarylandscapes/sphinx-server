:orphan:

####################
Document Conventions
####################

*************************
Typographical Conventions
*************************

To make the document easier to understand and follow, the following typographical conventions will be used.

.. tabularcolumns:: |p{\dimexpr 0.2\linewidth-2\tabcolsep}|p{\dimexpr 0.7\linewidth-2\tabcolsep}|

.. list-table:: Typographical Conventions
    :header-rows: 1
    :widths: 1,3

    * - Typeface
      - Description
    * - AabBCc123
      - Variable width font. Normal Paragraph text or instruction.
    * - :samp:`AabBCc123 {AabBCc123}`
      - Fixed width font, different background.  Text to be entered or shown on the screen.  All commands will be shown with the appropriate shell prompt in front of them (eg :samp:`#`).  Text in :samp:`{italics}` indicates that it is a variable for the user to replace.
    * - :kbd:`F2` or :kbd:`alt+h`
      - Fixed width font, different background.  A key, or sequence of keys, to press.  Eg :kbd:`F2` or :kbd:`alt+h`.  For a sequence of keys, they should be pressed at the same time.
    * - :guilabel:`AabBCc123`
      - Variable width font, italics. This represents an on screen :term:`GUI` element: label, text field, button, etc
    * - :menuselection:`&Menu1 --> &Option1`
      - The same font as for :term:`GUI` labels, but with an arrow between elements.  This format indicates the sequence of button presses or mouse clicks used to navigate through :term:`GUI` elements.  If a letter in the sequence is underlined, that indicates the hot key for that menu, which is typically accessed by pressing :kbd:`alt` + the underlined key to open the initial menu.  For example in the following sequence :menuselection:`&File --> &Save`, you can press :kbd:`alt+f` to open the :guilabel:`File` menu then :kbd:`s` to activate the :guilabel:`Save` function.
    * - :program:`Microsoft Word`
      - Bold, variable width font.  The name of an application.


In addition to the type faces, this document will also use the ``\`` or ``^`` characters as line continuation characters. The backslash ``\`` is used for Unix systems and the caret ``^`` is used for Windows systems. Be sure to leave one space between the comand and the line continuation characters to prevent entering an invalid command. Some command lines will not fit on a single line across this page, so in order to be clear about the syntax, any command that spans more than one line in this document will have a ``\``  or ``^`` where the line continues and each line that is part of the same command will be indented.  The operator has the choice of typing in the command exactly as shown on Unix or Windows based systems (which is still valid Unix shell or Windows Command Prompt syntax) or removing the line continuation character and entering it as one long line, which is required for all Network and Network Appliance devices.  Below are examples of commands with line continuations:

Unix/Linux system:

.. code-block:: shell-session

    # /usr/cluster/bin/scinstall -i -C app-c -F -o -G lofi \
        -P task=quorum,state=INIT \
        -e global_fencing=nofencing

Windows system:

.. code-block:: shell-session

    > fc /l "C:\Program Files (x86)\file1.txt" ^
            "C:\Program Files (x86)\file2.txt"

*****************
Shell Conventions
*****************

.. tabularcolumns:: |p{\dimexpr 0.2\linewidth-2\tabcolsep}
                    |p{\dimexpr 0.7\linewidth-2\tabcolsep}|

.. list-table:: Shell Conventions
   :header-rows: 1
   :widths: 1,3

   * - Sample Prompt
     - Description

   * - :samp:`#`
     - Normal UNIX Shell prompt ending in a :samp:`#`, this is the shell of the root user

   * - :samp:`$`
     - Normal UNIX Shell prompt ending in a :samp:`$`, this is the shell of a non-privileged user

   * - :samp:`>`
     - UNIX shell line continuation prompt ending in a :samp:`>`.

   * - :samp:`{C}:{\\}>`
     - Shell prompt beginning with a drive letter, followed by the current path, and ending in :samp:`>`.

       This is a command prompt for Windows shells

   * - :samp:`hostname>`
     - Cisco Device prompt ending in :samp:`>`, this is the shell of a user

   * - :samp:`hostname#`
     - Cisco Device prompt ending in :samp:`#`, this is the shell of a privileged user

   * - :samp:`hostname(config)#`
     - Cisco Device prompt ending in :samp:`(config)#`.

       This is the shell of a privileged user in global configuration mode


*******************
Windows GUI Buttons
*******************

Throughout this document, several :term:`GUI` commands are given that are explained here. Whenever this document asks for the following commands, the descriptions below describe how to find the command.

.. |ps_icon| image:: PowerShellPrompt.jpg
    :height: 5mm
    :width: 5mm
    :alt: Windows 10/2012/2016 Power Shell button

.. |sm_icon| image:: ServerManager.png
    :height: 5mm
    :width: 5mm
    :alt: Windows 2012/2016 Server Manager button

.. |start_icon_win2012| image:: start_win2012.png
    :height: 5mm
    :width: 5mm
    :alt: Windows 10/2012/2016 Start button

.. |start_icon_win7| image:: start_win7.png
    :height: 5mm
    :width: 5mm
    :alt: Windows 7 Start button

* **Start** In the task bar at the bottom of the desktop, click on the Start icon (|start_icon_win2012| for Windows 2012 or |start_icon_win7| for Windows 7)

* **Launch PowerShell** In the task bar at the bottom of the desktop, right-click on the Windows PowerShell icon (|ps_icon|) and select the appropriate account

* **Launch Server Manager** In the task bar at the bottom of the desktop, click on the Server Manager icon (|sm_icon|)


******************
``root`` as a Role
******************

In Solaris 11 or Red Hat Enterprise Linux, the ``root`` account is no longer a user, instead it is a role.  This means you cannot directly login as user ``root``.  Instead you must login as an authorized user.  In this procedure you can login as user ``install`` (or any other account with ``sudo`` rights).  In order to assume the ``root`` role, run the command:

.. code-block:: shell-session

    # sudo su -

This will prompt you for a password, enter the password for the current user.


.. raw:: latex

    \newpage
