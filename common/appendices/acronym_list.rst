:orphan:

############
Acronym List
############

.. glossary::

    ACL
        Access Control List

    AD
        Active Directory

    ADCS
        Active Directory Certificate Services

    APS
        Advanced Program System

    ASM
        Automatic Storage Management

    AU
        Allocation Unit

    AZS
        Alternate Zone Suffix

    BBU
        Battery Backup Unit

    BE
        Boot Environment

    BIOS
        Basic Input Output System

    CA
        Certificate Authority

    CAM
        Common Array Manager

    CAS
        Client Access Server (Exchange context)

    CADC
        Conflict Avoidance Display Control (CSCI)

    CD
        Compact Disk

    CIFS
        Common Internet File System

    COE
            Common Operating Environment

    COTS
        Commercial Off The Shelf

    CRL
        Certificate Revocation List

    CSCP
        Communication Server Control Panel (Lync Server Control Panel)

    CSSH
        Cluster Secure Shell

    CSCI
        Computer Software Configuration Item

    DB
        Database

    DBA
        Database Administration

    DG
        Disk Group

    DG
        Distribution Group (Lync and Exchange context)

    DHCP
        Dynamic Host Configuration Protocol

    DNS
        Domain Name System

    DoD
        Department of Defense (United States)

    DVD
        Digital Versatile Disk

    EMS
        Exchange Management Shell

    ER
        Engineering Release

    EULA
        End User Licensing Agreement

    EWS
        Exchange Web Services

    EXUM
        Exchange Unified Messaging

    FC
        Fiber Channel

    FOB
        Forward Operating Base

    FM
        Flight Management

    FQDN
        Fully Qualified Domain Name

    FRA
        Flash Recovery Area

    GAL
        Global Address List

    GB
        Gigabyte

    GPO
        Group Policy Object

    GRUB
        GRand Unified Bootloader

    GUI
        Graphical User Interface

    HA
        High Availability

    HAMRS
        Highly Available Managerial Resource Servers

    HBA
        Host Bus Adapter

    HDD
        Hard Disk Drive

    HSM
        Hieratical File System also known as :term:`SAM-QFS`

    IC
      Interoperability Components (CSCI)

    IFC
        Integrated Functional Capability

    IIS
        Internet Information Services

    ILOM
        Integrated Lights Out Management

    IM
      Information Management (CSCI)

    IP
        Internet Protocol

    IPC
        Inter-Process Communication

    IPS
        Image Packaging System.  The package file format used by Solaris 11.

    IPT
        Integrated Product Team

    IPv4
        Internet Protocol Version 4

    IPv6
        Internet Protocol Version 6

    ISO
        International Organization for Standardization.  Also short for
        :term:`iso9660`.

    ISO9660
        A file system standard published by the International Organization for Standardization (ISO) for optical disc media.

    JDBC
        Java Database Connectivity

    KB
        Kilobyte

    KMS
        Key Management Service

    LAN
        Local Area Network

    LDAP
        Lightweight Directory Access Protocol

    LM
        Logistics Management (CSCI)

    LRU
        Line Replaceable Unit

    LUN
        Logical Unit

    MAPI
        Messaging Application Programming Interface

    MB
        Megabyte

    MCE
        Mission Control Element

    MNTR
        Monitor Database

    MOS
        My Oracle Support

    MOB
        Main Operating Base

    MP
        Mission Planning

    MRAS
        Media Relay Authentication Service

    NETCA
        Network Configuration Assistant

    NFS
        Network File System

    NGC
        Northrop Grumman Corporation

    NIC
        Network Interface Card

    NTP
        Network Time Protocol

    OCI
        Oracle Call Interface

    OCR
        Oracle Cluster Registry

    OCSP
        Online Certificate Status Protocol

    OEM
        Oracle Enterprise Manager

    OMS
        Oracle Management Server

    OPER
        Operational Database

    OS
        Operating System

    OVA
        Open Virtual Appliance

    POST
        Power On Self Test

    PHM
        Prognostics health Management (CSCI)

    QFS
        Quick File System

    RAC
        Real Application Cluster

    RAID
        Redundant Array of Inexpensive Disks

    RE
        Responsible Engineer

    RSA
        Rivest Shamir Adelman Algorithm

    SAM-QFS
        Storage Archive Manager-Quick File System also known as :term:`HSM`

    SAN
        Storage Area Network

    SAS
        Serial Attached SCSI

    SATA
        Serial Advanced Technology Attachment

    SCAN
        Single Client Access Network

    SCP
        Service Connection Point or Secure Copy (part of the :term:`SSH` suite)

    SCSI
        Small Computer Standard Interface

    SFTP
        Secure File Transfer Protocol (part of the :term:`SSH` suite)

    SGA
        System Global Area

    SHA
        Secure Hash Algorithm

    SIP
        Session Initiation Protocol

    SMB
        Server Message Blocks

    SMTP
        Simple Mail Transfer Protocol

    SP
        Service Processor (Oracle hardware context)

    SP
        Service Pack (Windows context)

    SRV
        Service Record in DNS

    SSD
        Solid State Disk

    SSH
        Secure Shell

    TB
        Terabyte

    TBD
        To Be Determined

    TCP
        Transmission Control Protocol

    TFTP
        Trivial File Transfer Protocol

    TLS
        Transport Layer Security

    TNS
        Transparent Network Substrate

    UAR
        Universal Archive, Solaris 11 Backup/Recover Format

    UAVC2
        Unmanned Air Vehicle Command and Control (CSCI)
        
    UCS
        Unified Contact Store

    UDP
        User Datagram Protocol

    URI
        Uniform Resource Identifier

    URL
        Uniform Resource Locator

    VM
        Virtual Machine

    WDS
        Windows Deployment Service

    WSUS
        Windows Server Update Services

    WWN
        World Wide Name (Unique Identifier)

    WSUS
        Windows Server Update Services

    ZFS
        Zetabyte File System
