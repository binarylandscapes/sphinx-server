:orphan:

.. jinja:: yaml_config

    ###########################
    Git Changelog for Document
    ###########################

    .. _git_changelog_doc:

	{% if 'inGit' in gitstatus %}

		.. git_changelog::

	{% else %}

	**Not rendered due to document not being part of a Git repository.**

	{% endif %}

	.. raw:: latex

		\newpage
