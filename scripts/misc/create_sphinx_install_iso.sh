#!/bin/bash
# Create ISO file of working cygwin/sphinx install dir on hdd.
# Ensure you have update sum.txt prior and update dash of disc in volume label and output filename.

mkisofs -r -d -D -J -U -udf --iso-level 4 -V SPHINXDVD0001-002 -o ./SPHINXDVD0001-002.iso ./SPHINX_01_07_02
sha1sum ./SPHINXDVD0001-002.iso >> SPHINXDVD0001-002_sha1sum.txt
