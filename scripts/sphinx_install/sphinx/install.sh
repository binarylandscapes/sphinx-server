#!/bin/bash

echo Setting up Cygwin environment.
echo
if [[ ! -f /etc/passwd ]]
then
    echo "/etc/passwd doesn't exist, creating it now.  This can take a long time"
    echo
    mkpasswd > /etc/passwd
fi

if [[ ! -f /etc/group ]]
then
    echo "/etc/group doesn't exist, creating it now.  This can take a long time"
    echo
    mkpasswd > /etc/group
fi

#Copy additional files for required extentions (plantuml)
if [[ ! -f /java/plantuml.jar ]]
then
    echo "Copying additional files for plantuml and configuring settings"
    echo
    cp -r ./java/. /java/
    cp -r ./graphviz-2.38/. /graphviz-2.38/
	chmod -R 0777 /graphviz-2.38
	chmod -R 0777 /java
    echo "alias plantuml='cygstart /java/plantuml.jar'" >> ~/.bashrc
    export GRAPHVIZ_DOT="/graphviz-2.38/release/bin/dot"
fi

source ~/.bashrc
echo Cygwin environment setup is Complete

echo Starting installation of Sphinx and components in Cygwin
# Install Sphinx and the extensions we use at approved versions
echo
echo "Installing Sphinx, its extensions, and other requirements."
    python -m pip install --upgrade \
    --no-index \
    --find-links=sphinx_whls \
    sphinx==1.7.2 \
    sphinx-jinja==0.3.0 \
    sphinx-autobuild==0.7.1 \
    netaddr==0.7.19 \
    nwdiag==1.0.4 \
    actdiag==0.5.4 \
    seqdiag==0.9.5 \
    blockdiag==1.5.3 \
    yaml_config==1.3.3 \
    gitpython==2.1.9 \
    sphinxcontrib-seqdiag==0.8.5 \
    sphinxcontrib-nwdiag==0.9.5 \
    sphinxcontrib-blockdiag==1.5.5 \
    sphinxcontrib-actdiag==0.8.5 \
    reportlab==3.4.0 \
    sphinx-git==10.1.1 \
    sphinx_rtd_theme==0.2.4 \
    plantuml==0.1.1 \
    sphinxcontrib-plantuml==0.10 \
    tablib==0.12.1 \
    ciscoconfparse==1.3.10 \
    yml2json==1.1.3 \
    sphinxcontrib-jupyter==0.1.dev20171024 \
    sphinxcontrib_ansibleautodoc==0.0.5 \
    sphinxcontrib-jsonschema==0.9.3 \
    sphinxcontrib-confluencebuilder==0.8 \


echo
if [[ $? != 0 ]]
then
    echo "Installation of Sphinx+extensions failed."
    exit 1
fi

# This is required to make forms or digital sigs in PDFs
if [[ -z $(ls -l /usr/share/texmf-dist/tex/latex/acrotex/* 2>/dev/null) ]]
then
    echo
    echo "Installing acrotex for PDF Forms support..."
    cwd=$(pwd)
    temp_dir=$(mktemp -d)
    unzip -qod ${temp_dir} acrotex/acrotex.zip
    cd ${temp_dir}/acrotex
    for f in $(ls -1 *.ins)
        do latex $f
    done
    mkdir -p /usr/share/texmf-dist/tex/latex/acrotex
    cp *.sty *.cfg *.def /usr/share/texmf-dist/tex/latex/acrotex
    mktexlsr
    cd ${cwd}
    rm -r ${temp_dir}
 fi
echo
echo Cygwin Sphinx Installation is Complete
echo
echo Copying Sphinx testing files to Cygwin
echo
rm -rf /baseline_sphinx_env/
cp -r ./baseline_sphinx_env/. /baseline_sphinx_env/
chmod -R 0777 /baseline_sphinx_env/*

# CM Capture of installation
echo Performing Cygwin CM Capture to /cygwin_pip_installed_list.txt and /cygwin_pip_installed_list.txt
echo
rm /cygwin_pip_installed_list.txt
rm /cygwin_installed_list.txt
python -m pip list --format=columns >> /cygwin_pip_installed_list.txt
cygcheck --version >> /cygwin_installed_list.txt
cygcheck --check-setup >> /cygwin_installed_list.txt
