#!/bin/bash

# Builds Sphinx Test Document
echo Running Sphinx Installation test
echo When you see "Serving on http://127.0.0.1:8000" in Cygwin window, hit refresh on browser it opens.
cd /baseline_sphinx_env/NG0000P0001_Sphinx_Manual
make clean latexpdf
# Opens Sphinx built PDF
cygstart /baseline_sphinx_env/NG0000P0001_Sphinx_Manual/build/latex/NG0000P0001_Rev_NC_Sphinx_Manual_01.07.02.pdf
# Builds Sphinx Live HTML & Opens Default Web Browser to SphinxLive HTML Server
cygstart http://127.0.0.1:8000/ ; make livehtml
