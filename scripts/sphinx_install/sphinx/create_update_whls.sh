#!/bin/bash

source ./proxy

# Create wheels for required Sphinx pip installs from pip_requirements_sphinx.txt file
echo
echo "Downloading and Creating/Updating wheels for offline Sphinx install in /sphinx_whls. from pip_requirements_sphinx.txt file"
echo
python -m pip wheel --process-dependency-links --wheel-dir=./sphinx_whls --requirement ./pip_requirements_sphinx.txt
