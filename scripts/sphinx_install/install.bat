@ echo off
echo Expecting location of Cygwin Installer disc to be %~d0
set drive=%~d0
set driveLetter=%drive:~0,-1%
:: Install Cygwin
echo Starting Cygwin Installation...
cd %~d0\cygwin64_install\
call install_cygwin64.bat
setx GRAPHVIZ_DOT "C:\cygwin64\graphviz-2.38\release\bin\dot.exe"
setx PATH "C:\cygwin64\graphviz-2.38\release\bin\;%PATH%"
echo Cygwin Installation is Complete.
echo To perform installation of Sphinx and components in Cygwin - Required
echo Leave media installed and run Cygwin from desktop shortcut and execute "cd /cygdrive/%driveletter%/sphinx ; ./install.sh"
cd C:\
pause
